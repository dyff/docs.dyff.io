=====
Types
=====

Since we're using TypeScript and Dyff API uses Python (and pydantic models for typing), we need to bridge the gap between language types.

Open API Types
--------------

We can generate types from the Dyff API openapi.json schema using the ``openapi-typescript`` package:

.. code-block:: bash

    npx openapi-typescript <DYFF_API_URL>/openapi.json -o ./types/dyff-api.d.ts

``DYFF_API_URL`` can be either the production or dev deployment of dyff-api. Types will be output to ``dyff-api.d.ts`` in the types folder

.env file typing
----------------

Environment variable types are set in ``.env`` are defined in ``env.d.ts`` in the project root
