=============
Dyff Frontend
=============

The dyff frontend plugs into the Dyff API to facilitate basic CRUD operations and evaluation visualization.

.. toctree::

   environment.rst
   types.rst
   testing.rst
