Modules
========================================================

Create and upload a Module
-------------------------------------------------

``Modules`` are Dyff's mechanism for allowing users to upload their own analysis
code. A :class:`~dyff.schema.platform.Module` is just a directory tree containing code
files. You create a ``Module`` in two steps, basically the same way as a
:class:`~dyff.schema.platform.Dataset`. First create a Dyff ``Module`` containing
a Python code package. Dyff assigns a unique ID to the module and returns a full
Module object. Second, upload the actual package, providing the Module object so
that Dyff knows where to store the data.

Assuming you've implemented your method in a file called
``examples/my_package.py`` in the directory ``/home/me/dyff/my-module``, you
would create and upload the package like this:

.. code-block:: python

    from dyff.client import Client

    dyffapi = Client()

    module = dyffapi.modules.create_package(
        "/home/me/dyff/my-module", account=ACCOUNT, name="my-module"
    )
    dyffapi.modules.upload_package(module, "/home/me/dyff/my-module")


If you created the module resource but couldn't complete the upload, you can
fetch the module record and re-try the upload:
``module = dyffapi.modules.get(<module.id>)``
