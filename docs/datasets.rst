Datasets
=======================================================

Create and Upload a Dataset
---------------------------

To use an Arrow dataset as a new Dyff Dataset requires two steps.

First, you create a ``Dataset`` record. Dyff assigns a unique ID to the dataset
and returns a full ``Dataset`` object.

Second, you upload the actual data, providing the ``Dataset`` object so that
Dyff knows where to store the data. The hashes of the dataset files must match
the hashes calculated in the ``create`` step.

Setup
-----

Create an API client as described in the :doc:`Python client guide
<../../connect>`:

.. tab-set::

    .. tab-item:: API Client

        .. code-block:: python

            from dyff.client import Client

            dyffapi = Client()

    .. tab-item:: DyffLocalPlatform

        .. code-block:: python

            from dyff.audit.local import DyffLocalPlatform

            dyffapi = DyffLocalPlatform()


.. code-block:: python

    dataset = dyffapi.datasets.create_arrow_dataset(
        ARROW_DATASET_ROOT_DIRECTORY, account=ACCOUNT, name=DATASET_NAME
    )
    print(f"created dataset:\n{dataset}")

    dyffapi.datasets.upload_arrow_dataset(dataset, ARROW_DATASET_ROOT_DIRECTORY)


If you created the dataset but couldn't complete the upload, you can
fetch the dataset record and re-try the upload:
``dataset = dyffapi.datasets.get(<dataset.id>)``
