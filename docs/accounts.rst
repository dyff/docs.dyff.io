Account management
==================

Accounts are managed using a CLI tool implemented as a Python module. The CLI
tool is part of the ``dyff-api`` package. The entrypoint is:

.. code-block:: console

    $ python3 -m dyff.api.mgmt

To use the CLI tool in a running cluster, log in to one of the ``dyff-api`` pods
with a command like this:

.. code-block:: console

    $ kubectl exec -it -n dyff-api dyff-api-6576fbf469-4vwkz -- bash -il

Then, run the CLI tool in the pod.

Create an account
-----------------

An account can be created using the ``accounts create`` subcommand:

.. code-block:: console

    $ python3 -m dyff.api.mgmt accounts create -n user.name@example.com
    created account: 'user.name@example.com' (3076c505c36b46bdad1cf1abefaeb580)

Creating an account grants no access to the Dyff application. For that, a token
is necessary.

Create a token
--------------

A token must be generated to access the Dyff API. When creating a token, you
need to specify the permissions that it grants.

.. warning::

    Avoid creating long-lived highly-privileged tokens in a production
    environment. Instead, if you need elevated privileges, create short-lived
    ephemeral tokens with just the permissions you need.

There are three ways to specify permissions. On successful creation, the API
token will be printed to the console.

Using "roles"
'''''''''''''

.. warning::

   The ``Admin`` role grants permission to do any operation on any resource in
   any account.

There are a few pre-defined "roles" that cover common cases. The available roles
are:

    ``Admin``
        Grants all permissions

    ``AuditDeveloper``
        Grants full permissions within the associated account, read permissions
        on all resources, and consume permissions on ``public`` resources.

To create a token using a role:

.. code-block:: bash

    python3 -m dyff.api.mgmt tokens create -t account -i aeb78193c526484fa4f5c14d182ba039 --role AuditDeveloper

Via command line switches
'''''''''''''''''''''''''

You can create a token via command line options. Here, we grant the account with
ID ``aeb78193c526484fa4f5c14d182ba039`` permission to consume all datasets and
models in the ``test`` account:

.. code-block:: bash

    python3 -m dyff.api.mgmt tokens create -t account -f 'consume' -a 'test' -r 'datasets,models' -i aeb78193c526484fa4f5c14d182ba039

Via a config file
'''''''''''''''''

You can also create a token via a config file. This example specifies the same
permissions as the previous one using a config file:

.. code-block:: yaml
    :caption: policy.yaml

    account: aeb78193c526484fa4f5c14d182ba039
    grants:
    - resources:
      - datasets
      - models
      functions:
      - consume
      accounts:
      - test

Then pass the config file to the CLI:

.. code-block:: bash

    python3 -m dyff.api.mgmt tokens create -t account -c config.yaml

When necessary, the config file may also be provided through ``stdin``:

.. code-block:: bash

    python3 -m dyff.api.mgmt tokens create -t account -c - <<EOF
    account: aeb78193c526484fa4f5c14d182ba039
    grants:
    - resources:
      - datasets
      - models
      functions:
      - consume
      accounts:
      - test
    EOF
