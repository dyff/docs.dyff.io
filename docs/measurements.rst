Measurements
============

Initialize a Client
-------------------

.. code-block:: python

    from dyff.client import Client

    dyffapi = Client()


Create a Measurement workflow
-----------------------------

.. code-block:: python

    from dyff.client import Client
    from dyff.schema.requests import AnalysisArgument, AnalysisCreateRequest, AnalysisInput

    ACCOUNT: str = ...
    # The ID of an existing Method to apply
    METHOD_ID: str = ...

    dyffapi = Client()

    # Example arguments / inputs. These are specified by the Method.
    request = AnalysisCreateRequest(
        account=ACCOUNT,
        method=METHOD_ID,
        arguments=[
            AnalysisArgument(keyword="temperature", value="1.0"),
        ],
        inputs=[
            AnalysisInput(keyword="prompts", entity=dataset_id),
            AnalysisInput(keyword="completions", entity=evaluation_id),
        ],
    )

    measurement = dyffapi.measurements.create(request)
