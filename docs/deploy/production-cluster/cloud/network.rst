Network
=======

Create the VPC:

.. code-block:: terraform

    resource "google_compute_network" "dyff" {
      name                            = "dyff"
      delete_default_routes_on_create = false
      auto_create_subnetworks         = false
      routing_mode                    = "REGIONAL"
    }

Create private subnet and assign addresses to all the zones in the region:

.. code-block:: terraform

    resource "google_compute_subnetwork" "dyff" {
      name          = "${local.name}-subnetwork"
      ip_cidr_range = "10.0.0.0/16"
      region        = local.region
      network       = google_compute_network.dyff.id

      private_ip_google_access = true

      secondary_ip_range {
        range_name    = "pod-ranges"
        ip_cidr_range = "192.168.0.0/18"
      }

      secondary_ip_range {
        range_name    = "services-range"
        ip_cidr_range = "192.168.64.0/18"
      }
    }

.. code-block:: terraform

    resource "google_compute_router" "dyff" {
      name    = "${local.name}-router"
      region  = local.region
      network = google_compute_network.dyff.id
    }

.. code-block:: terraform

    resource "google_compute_router_nat" "dyff" {
      name                               = "${local.name}-router-nat"
      router                             = google_compute_router.dyff.name
      region                             = local.region
      nat_ip_allocate_option             = "AUTO_ONLY"
      source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

      subnetwork {
        name                    = google_compute_subnetwork.dyff.name
        source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
      }
    }
