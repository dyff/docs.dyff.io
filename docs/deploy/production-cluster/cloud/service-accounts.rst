Service accounts
================

The Google provider must be configured with a service account. We recommend
creating a separate service account per Terraform workspace. For the simplest
scenario, this translates to:

- One account for cloud resources

- One account for cluster resources

Cloud service account
---------------------

The service account to manage cloud resources needs the following roles:

- ``roles/cloudkms.admin``

- ``roles/compute.admin``

- ``roles/container.admin``

- ``roles/container.clusterAdmin``

- ``roles/iam.serviceAccountAdmin``

- ``roles/iam.serviceAccountUser``

- ``roles/resourcemanager.projectIamAdmin``

- ``roles/storage.admin``

- ``roles/storage.hmacKeyAdmin``

.. _cluster-service-account:

Cluster service account
-----------------------

The service account to manage cluster resources needs the following roles:

- ``roles/compute.networkViewer``

- ``roles/container.admin``

Configure the service account
-----------------------------

Add a variable to inject the credentials into the configuration:

.. code-block:: terraform

    variable "google_cloud_service_account_file" {
      type = string
    }

Pass the variable to the provider:

.. code-block:: terraform

    provider "google" {
      credentials = file(var.google_cloud_service_account_file)
      project     = "example"
    }
