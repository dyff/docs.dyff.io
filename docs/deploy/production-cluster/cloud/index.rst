Cloud resources
===============

This section discusses the provisioning of the underlying cloud resources where
Dyff will be operated.

At a minimum, two Terraform workspaces are needed to operate a Dyff
deployment:

- One to deploy the cluster, and

- One to deploy resources into it.

We recommend using one Git repository for each workspace, but other management
strategies are possible.

.. toctree::
    :maxdepth: 2

    service-accounts
    network
    cluster
    storage

Requirements
------------

Add locals for region and name.

.. code-block:: terraform

    locals {
      name       = "dyff"
      region     = "us-central1"
    }

The following providers are needed:

.. code-block:: terraform

    terraform {
      required_providers {
        google = {
          source  = "hashicorp/google"
          version = "~> 5.18.0"
        }
        random = {
          source  = "hashicorp/random"
          version = "~> 3.5.1"
        }
      }
      required_version = ">=1.0"
    }

Enable Cloud APIs
-----------------

The following Google Cloud APIs must be enabled, at a minimum:

- ``cloudkms.googleapis.com``

- ``cloudresourcemanager.googleapis.com``

- ``container.googleapis.com``

- ``containersecurity.googleapis.com``

- ``iam.googleapis.com``

- ``identitytoolkit.googleapis.com``

- ``logging.googleapis.com``

- ``networkmanagement.googleapis.com``

- ``servicedirectory.googleapis.com``

- ``serviceusage.googleapis.com``

- ``storage.googleapis.com``

- ``storagetransfer.googleapis.com``
