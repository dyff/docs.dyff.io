Cluster
=======

Add a local for the cluster name:

.. code-block:: terraform

    locals {
      cluster_name = "${local.name}-cluster"
    }

Create a service account for cluster nodes to use:

.. code-block:: terraform

    resource "google_service_account" "cluster_node" {
      account_id   = "tf-${local.deployment}-cluster-node"
      display_name = "tf-${local.deployment}-cluster-node"
    }

.. # https://cloud.google.com/kubernetes-engine/docs/how-to/service-accounts#default-gke-service-agent
.. # https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster#use_least_privilege_sa

Grant the appropriate permissions to the cluster node service account to enable
GKE features:

.. code-block:: terraform

    resource "google_project_iam_member" "cluster_node" {
      for_each = toset([
        "roles/autoscaling.metricsWriter",
        "roles/logging.logWriter",
        "roles/monitoring.metricWriter",
        "roles/monitoring.viewer",
        "roles/stackdriver.resourceMetadata.writer",
      ])
      role    = each.key
      member  = "serviceAccount:${google_service_account.cluster_node.email}"
    }

Serial port logging is required by GKE Autopilot for normal operation:

.. https://cloud.google.com/kubernetes-engine/docs/troubleshooting/autopilot-clusters#scale-up-failed-serial-port-logging

.. code-block:: terraform

    resource "google_compute_project_metadata_item" "enable_serial_port_logging" {
      key   = "serial-port-logging-enable"
      value = "true"
    }

Create the cluster:

.. code-block:: terraform

    resource "google_container_cluster" "dyff" {
      name     = local.cluster_name
      location = local.region

      network    = google_compute_network.dyff.id
      subnetwork = google_compute_subnetwork.dyff.id

      enable_autopilot    = true

      cluster_autoscaling {
        auto_provisioning_defaults {
          management {
            auto_upgrade = true
            auto_repair  = true
          }

          service_account = google_service_account.cluster_node.email
        }
      }

      ip_allocation_policy {
        cluster_secondary_range_name  = "pod-ranges"
        services_secondary_range_name = "services-range"
      }

      private_cluster_config {
        enable_private_endpoint = false
        enable_private_nodes    = true
      }
    }
