workflows-aggregator
====================

``workflows-aggregator`` retrieves the outputs of the ``kafka`` cluster resource.

Deploy the `workflows-aggregator` Helm chart:

.. code-block:: bash

	helm install workflows-aggregator oci://registry.gitlab.com/dyff/charts/workflows-aggregator --version 0.3.7 \
	--set extraEnvVarsConfigMap.DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS=<kafka bootstrap server> \
	--set extraEnvVarsConfigMap.DYFF_KAFKA__STREAMS__APPLICATION_ID=<kafka stream application id> \
	--set extraEnvVarsConfigMap.DYFF_KAFKA__STREAMS__STATE_DIR=<kafka/state/directory>

To know the latest version `workflows-aggregator` to install, go to the `workflows-aggregator`
`Artifact Hub <https://artifacthub.io/packages/helm/workflows-aggregator/workflows-aggregator>`_
