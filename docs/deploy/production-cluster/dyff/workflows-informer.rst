workflows-informer
==================

``workflows-informer`` propagates the cluster's resource status changes to Kafka

Deploy the `workflows-informer` Helm chart:

.. code-block:: bash

	helm install workflows-informer oci://registry.gitlab.com/dyff/charts/workflows-informer --version 0.4.2 \
	--set extraEnvVarsConfigMap.DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS=<kafka bootstrap server>

To know the latest version `workflows-informer` to install, go to the `workflows-informer`
`Artifact Hub <https://artifacthub.io/packages/helm/workflows-informer/workflows-informer>`_
