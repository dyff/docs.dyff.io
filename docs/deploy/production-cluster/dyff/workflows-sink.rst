workflows-sink
==============

``workflows-sink`` retrieves the outputs of the ``kafka``, ``mongodb``, and
``storage`` cluster resources.

Deploy the `workflows-sink` Helm chart:

.. code-block:: bash

  helm install workflows-aggregator oci://registry.gitlab.com/dyff/charts/workflows-sink --version 0.4.5 \
	--set extraEnvVarsConfigMap.DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS=<kafka bootstrap server> \
	--set extraEnvVarsConfigMap.DYFF_KAFKA__CONFIG__GROUP_ID=<kafka config group id> \
	--set extraEnvVarsConfigMap.DYFF_WORKFLOWS_SINK__SINK_KIND="mongodb" \
	--set extraEnvVarsConfigMap.DYFF_WORKFLOWS_SINK__MONGODB__DATABASE=<mongodb database credentials>

To know the latest version `workflows-sink` to install, go to the `workflows-sink`
`Artifact Hub <https://artifacthub.io/packages/helm/workflows-sink/workflows-sink>`_

.. code-block:: terraform
    :force:

    set_sensitive {
      name  = "extraEnvVarsSecret.DYFF_WORKFLOWS_SINK__MONGODB__CONNECTION_STRING"
      value = local.mongodb.credentials["command"].connection_string_escaped
    }
