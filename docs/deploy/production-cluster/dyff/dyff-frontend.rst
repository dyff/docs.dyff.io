dyff-frontend
=============
``dyff-frontend`` provides a web interface for the Dyff AI auditing platform.

Deploy the `dyff-frontend` Helm chart:

.. code-block:: bash

	helm install dyff-frontend oci://registry.gitlab.com/dyff/charts/dyff-frontend --version 0.2.0

To know the latest version `dyff-frontend` to install, go to the `dyff-frontend`
`Artifact Hub <https://artifacthub.io/packages/helm/dyff-frontend/dyff-frontend>`_

If you want the dyff-frontend URL to be something other than `https://api.dyff.io` add the following
flag to the helm install command:

.. code-block:: bash

	--set config.apiUrl=https://host-you-want:port


Set up `dyff-frontend` with Ingress nginx.

If you want to helm install with Ingress nginx, you will need to pass the
following flags in your `helm install` command:

.. code-block:: bash

	--set ingress.enabled=true
	--set ingress.className=nginx
	--set ingress.hosts[0].host=host-you-want.com

Additionally, to set up tls for Ingress, pass the following flags in the
`helm install` command:

.. code-block:: bash

	--set ingress.tls[0].secretName=dyff-frontend-tls
	--set ingress.tls[0].hosts[0]=host-you-want.com
