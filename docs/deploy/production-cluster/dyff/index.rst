Dyff platform
=============

This section walks through the provisioning of the Dyff platform.

This tutorial assumes you have already :doc:`created a Kubernetes cluster
<../cloud/index>` and :doc:`deployed required cluster services
<../cluster/index>`. If you have not completed either step, go back and complete
it now.

.. toctree::
    :maxdepth: 2

    dyff-api
    dyff-frontend
    dyff-orchestrator
    dyff-operator
    workflows-aggregator
    workflows-informer
    workflows-sink
