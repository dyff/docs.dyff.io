dyff-orchestrator
=================

``dyff-orchestrator`` retrieves the outputs of the ``kafka`` cluster resource.

Deploy the `dyff-orchestrator` Helm chart:

.. code-block:: bash

  helm install workflows-aggregator oci://registry.gitlab.com/dyff/charts/dyff-orchestrator --version 0.7.0 \
  --set extraEnvVarsConfigMap.DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS=<kafka bootstrap server> \
  --set extraEnvVarsConfigMap.DYFF_ORCHESTRATOR__IMAGES__HUGGINGFACE="registry.gitlab.com/dyff/workflows/huggingface-runner:0.1.0" \
  --set extraEnvVarsConfigMap.DYFF_ORCHESTRATOR__IMAGES__MOCK="registry.gitlab.com/dyff/workflows/inferenceservice-mock:0.1.0" \
  --set extraEnvVarsConfigMap.DYFF_ORCHESTRATOR__IMAGES__VLLM="registry.gitlab.com/dyff/workflows/vllm-runner:0.1.4"

To know the latest version `dyff-orchestrator` to install, go to the `dyff-orchestrator`
`Artifact Hub <https://artifacthub.io/packages/helm/dyff-orchestrator/dyff-orchestrator>`_
