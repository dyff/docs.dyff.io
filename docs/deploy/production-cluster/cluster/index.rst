Cluster resources
=================

This section walks through the provisioning of on-cluster resources to support
the Dyff platform.

This tutorial assumes you have already :doc:`created a Kubernetes cluster
<../cloud/index>`. If you have not, go back and complete it now.

.. toctree::
    :maxdepth: 2

    storage
    kafka
    mongodb
    ingress

Required providers
------------------

The following providers are needed:

.. code-block:: terraform

    terraform {
      required_providers {
        google = {
          source  = "hashicorp/google"
          version = "~> 5.18.0"
        }
        helm = {
          source  = "hashicorp/helm"
          version = "~> 2.12.0"
        }
        kubernetes = {
          source  = "hashicorp/kubernetes"
          version = "~> 2.27.0"
        }
        random = {
          source  = "hashicorp/random"
          version = "~> 3.5.1"
        }
      }
      required_version = ">=1.0"
    }

Provider setup
--------------

Add the Google Terraform provider with a :ref:`cluster service account
<cluster-service-account>`.

.. code-block:: terraform

    provider "google" {
      credentials = file("credentials.json")
      project     = "example"
    }

Configure the following data sources:

.. code-block:: terraform

    data "google_client_config" "provider" {}

    data "google_container_cluster" "dyff" {
      name     = local.cluster_name
      location = local.region
    }

Add the Helm and Kubernetes providers configured to use the Google cluster data
source:

.. code-block:: terraform

    provider "kubernetes" {
      host  = "https://${data.google_container_cluster.dyff.endpoint}"
      token = data.google_client_config.provider.access_token
      cluster_ca_certificate = base64decode(
        data.google_container_cluster.dyff.master_auth[0].cluster_ca_certificate,
      )
    }

    provider "helm" {
      kubernetes {
        host  = "https://${data.google_container_cluster.dyff.endpoint}"
        token = data.google_client_config.provider.access_token
        cluster_ca_certificate = base64decode(
          data.google_container_cluster.dyff.master_auth[0].cluster_ca_certificate,
        )
      }
    }
