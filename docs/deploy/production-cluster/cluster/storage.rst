Storage
=======

Ephemeral storage
-----------------

Disk storage
------------

Storage classes
^^^^^^^^^^^^^^^

Create a storage class with a reclaim policy of ``Retain``:

.. code-block:: terraform

    resource "kubernetes_storage_class" "retain_standard_rwo" {
      metadata {
        name = "retain-standard-rwo"
      }

      parameters = {
        "type" = "pd-balanced"
      }

      reclaim_policy      = "Retain"
      storage_provisioner = "pd.csi.storage.gke.io"
      volume_binding_mode = "WaitForFirstConsumer"
    }
