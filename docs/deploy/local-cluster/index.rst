Local cluster
=============

.. toctree::
    :hidden:

    cluster-host-setup
    cluster-setup
    cluster-validation
    tire-kicking

In this tutorial, we will walk through setting up a local Dyff development
cluster that can be used to develop service-level functionality and features for
Dyff.

Requirements
------------

The following is required to run a Dyff local cluster:

-   An x86-64 architecture. Dyff containers are currently only built for x86-64.

-   `Ubuntu 22.04 <https://www.releases.ubuntu.com/22.04/>`_ or newer, unless
    installing developer tools manually.

-   `Make <https://www.gnu.org/software/make/>`__ and `jq
    <https://jqlang.github.io/jq/>`__ are required to run the setup automation.

-   Root privileges are required to use Docker and edit system configuration.

Get the project
---------------

Check out the `dyff/dev-environment <https://gitlab.com/dyff/dev-environment/>`_
project.

.. code-block:: bash

    git clone https://gitlab.com/dyff/dev-environment.git
