Set up the cluster
==================

Create a ``kind`` cluster
-------------------------

A `kind <https://kind.sigs.k8s.io/>`__ config is included to deploy the
development cluster.

Running the following command will set up the cluster:

.. code-block:: bash

    make cluster

This creates a cluster called ``dyff`` using the
``cluster/kind/config.yaml`` config.

The following output will be desplayed to notify that the cluster has
been created:

.. code-block:: console

    $ make cluster
    Creating cluster "dyff" ...
     ✓ Ensuring node image (kindest/node:v1.27.3) 🖼
     ✓ Preparing nodes 📦
     ✓ Writing configuration 📜
     ✓ Starting control-plane 🕹️
     ✓ Installing CNI 🔌
     ✓ Installing StorageClass 💾
    Set kubectl context to "kind-dyff"
    You can now use your cluster with:

    kubectl cluster-info --context kind-dyff

    Thanks for using kind! 😊

Deploy cluster applications
---------------------------

Initialize Terraform by running:

.. code-block:: bash

    terraform init

Now Terraform is initialized, provision the cluster environment:

.. code-block:: bash

    terraform apply

Various credentials are output to a ``credentials.yaml`` file in the
project root.

Troubleshooting
---------------

.. warning:: This section is a stub. You can help us by expanding it.
