Kick the tires
==============

Enable Powerline
----------------

Add the following to the end of your ``~/.bashrc`` file to show more
context information at the command line:

.. code-block:: bash

   function _update_ps1() {
       PS1="$(/usr/local/bin/powerline-go -git-mode simple -cwd-mode plain -mode flat -modules user,host,cwd,gitlite,kube,terraform-workspace,venv -newline -error $?)"
   }

   if [ "$TERM" != "linux" ] && [ -f "/usr/local/bin/powerline-go" ]; then
       PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
   fi

Visualize MongoDB databases with Compass
----------------------------------------

.. code-block:: bash

    make compass

Some setup is required to connect to the local mongo database via
Compass.

In a terminal window, setup port forwarding to the mongo service:

.. code-block:: bash

    k port-forward -n mongodb svc/mongodb 27017

In Compass, under Advanced Connection Options -> Authorization,
reference the username and password in ``credentials.yaml`` generated
from ``terraform apply``:

.. code-block:: yaml

    "mongodb":
      "root":
        "password": "password_string" # pragma: allowlist secret
        "user": "user_string"

Visualize cluster resources with OpenLens
-----------------------------------------

`OpenLens <https://github.com/MuhammedKalkan/OpenLens>`__ can be used as
a local dashboard for the ``kind-dyff`` cluster:

.. code-block:: bash

    make open-lens

Monitor cluster metrics
-----------------------

Prometheus is installed as part of this deployment.

To see Prometheus metrics within OpenLens, go to *Settings* -> *Metrics*
for the ``kind-dyff`` cluster and configure the following:

-   **Prometheus**::

        Helm

-  **Prometheus Service Address**::

        prometheus/prometheus-server:80

At this point, metrics will begin to populate in OpenLens.

Access MinIO
------------

The MinIO web UI is available at
`minio.dyff.local <http://minio.dyff.local>`__.

The MinIO S3 API is available at
`s3.minio.dyff.local <http://s3.minio.dyff.local>`__.

You can use the MinIO Client to connect to MinIO. Run ``make mc`` if
it's not installed already.

.. code-block:: bash

    make mc

Configure an alias to connect to the MinIO API. The credentials can be
found in the ``credentials.yaml`` file:

.. code-block:: bash

    mc alias set minio http://s3.minio.dyff.local USERNAME PASSWORD

Try writing files to the pre-provisioned ``dyff`` bucket:

.. code-block:: bash

    mc cp README.md minio/dyff/
    mc ls minio/dyff/

Disable deployments
-------------------

In special cases such as performing development on ``dyff-api``, you may
want to disable a deployment of a dyff module in the dev environment,
and manually deploy that package.

For example, create a ``config.tfvars`` and disable `dyff-api` with the
following:

.. code-block:: hcl

    deploy_dyff_api = false

Re-apply terraform:

.. code-block:: bash

    make apply
