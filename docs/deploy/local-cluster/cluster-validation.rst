Validate the cluster
====================

In this step, we validate that the Dyff backend cluster is fully operational.

To validate the cluster, you'll need to install the `dyff
<https://pypi.org/project/dyff/>`_ package.

First, create a virtual environment:

.. code-block:: bash

    python3 -m venv venv
    . venv/bin/activate

Install ``dyff`` which installs ``dyff-audit``, ``dyff-client``, and
``dyff-schema``:

.. code-block:: bash

    python3 -m pip install dyff

Test that it's installed:

.. code-block:: bash

    python3 -c 'import dyff'

.. code-block:: console

    $ pip freeze | grep ^dyff
    dyff==0.13.0
    dyff-audit==0.2.1
    dyff-client==0.2.0
    dyff-schema==0.2.2

From the `dev-environment <https://gitlab.com/dyff/dev-environment>`_ root
directory, execute:

.. code-block:: bash

    ./scripts/test-deployment.sh local

This will create an admin account named ``default`` if one does not already
exist. It will then exercise the full audit pipeline by running simple versions
of each workflow. These test workflows will be created in the ``test`` account.
