Set up the cluster host
=======================

Add entries to ``/etc/hosts``
-----------------------------

.. tip::

    This step should be done on the system where you run your **web browser**.

Append the following host entries to the ``/etc/hosts`` file to make cluster
services accessible at the given hostnames on your system:

.. code-block:: bash

    sudo cat >> /etc/hosts <<EOF
    127.0.0.1       api.dyff.local
    127.0.0.1       minio.dyff.local
    127.0.0.1       s3.minio.dyff.local
    EOF

The applications these correspond to are not running yet, but we will be able to
verify them later.

Install development tools
-------------------------

The ``make install`` command will install the complete development
environment:

.. code-block:: bash

    make install

If ever the need arises, it is also possible to install each tool
individually:

.. code-block:: bash

    make docker
    make kind
    make kubectl

Add yourself to the ``docker`` group
------------------------------------

In order for the cluster setup to be able to run non-sudo
Docker commands, you must add your user to the ``docker`` group on your
system. In order to do that run the following:

.. code-block:: bash

    sudo usermod -a -G docker $(whoami)

Once you have added your user to the ``docker`` group, re-login for the
changes to take place.
