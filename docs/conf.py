# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

project = "Dyff docs"
copyright = "2024 Underwriters Laboratories Inc."
author = "Digital Safety Research Institute at UL Research Institutes"

extensions = [
    "nbsphinx",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.coverage",
    "sphinx.ext.doctest",
    "sphinx.ext.graphviz",
    "sphinx_copybutton",
    "sphinx_design",
    "sphinx_inline_tabs",
    "sphinx_sitemap",
    "sphinxcontrib.autodoc_pydantic",
    "sphinxext.opengraph",
]

highlight_language = "none"

autodoc_default_options = {
    "members": True,
    "undoc-members": True,
    "show-inheritance": True,
    "show-module-summary": True,
    "imported-members": True,
}
autodoc_member_order = "groupwise"
autodoc_typehints = "signature"

autodoc_pydantic_field_list_validators = False

autodoc_pydantic_model_summary_list_order = "alphabetical"
autodoc_pydantic_model_member_order = "groupwise"

autodoc_pydantic_model_show_config_summary = False
autodoc_pydantic_model_show_field_summary = False
autodoc_pydantic_model_show_json = False
autodoc_pydantic_model_show_validator_summary = False

autosummary_generate = True
# autosummary_imported_members = True

# FIXME: Temporarily excluding 'tutorials/' until we fix the content
exclude_patterns = ["tutorials/**", "_build", "Thumbs.db", ".DS_Store"]

copybutton_exclude = ".linenos, .gp"
copybutton_line_continuation_character = "\\"
copybutton_here_doc_delimiter = "EOF"
copybutton_selector = "div:not(.no-copy) > div.highlight > pre"

graphviz_output_format = "svg"

html_baseurl = "https://docs.dyff.io/"
html_title = "Dyff"
html_logo = "_static/logo.svg"
html_theme = "furo"
html_theme_options = {
    "announcement": (
        '<div class="admonition note"><p class="admonition-title">'
        "<strong>Pre-alpha software:</strong> "
        "Dyff is research code in a pre-alpha state. "
        "It is published without warranty, is subject to change at any time, "
        "and has not been certified, tested, assessed, or otherwise assured "
        "of safety by any person or organization. Use at your own risk."
        "</p></div>"
    ),
    "light_css_variables": {
        "font-stack": 'Arial, ui-sans-serif, system-ui, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
        "color-background-secondary": "#fff",
    },
    "source_repository": "https://gitlab.com/dyff/docs.dyff.io/",
    "source_branch": "main",
    "source_directory": "docs/",
}

html_static_path = ["_static"]

html_css_files = ["style.css"]

pygments_style = "nord"

ogp_site_url = "https://docs.dyff.io/"

sitemap_url_scheme = "{link}"

linkcheck_ignore = [
    r"https://github.com/",
    r"https://www.gnu.org/",
    r"^https?://.*?\.local.*$",
    # As of 20250123 this started returning 500 in linkcheck even though
    # the site is working
    r"https://jwt.io/",
]
