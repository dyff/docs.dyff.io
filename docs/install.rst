Installation
============

Install for use
---------------

Install via pip
^^^^^^^^^^^^^^^

Install the ``dyff`` package:

.. code-block:: bash

    python3 -m pip install dyff

To upgrade ``dyff``:

.. code-block:: bash

    python3 -m pip install --upgrade dyff

Install for development
-----------------------

The ``dyff`` package installs specific versions of the following
packages:

- ``dyff-audit``

- ``dyff-client``

- ``dyff-schema``

To work with the client code base, the ``dyff`` package must be uninstalled:

.. code-block:: bash

    python3 -m pip uninstall dyff

Then, install the packages to work on from source. For example, to develop
``dyff-audit``:

.. code-block:: bash

    git clone https://gitlab.com/dyff/packages/dyff-audit.git
    cd dyff-audit
    python3 -m pip install -e .

The above steps can be modified for the package of choice.
