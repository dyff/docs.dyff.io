===============
Dev Environment
===============

Check out the ``dyff-frontend`` project.

.. code-block:: bash

    git clone https://gitlab.com/dyff/dyff-frontend.git


Copy ``.env.example`` to a new ``.env`` file in project root. 

The example values point to a :doc:`local cluster deployment <../deploy/local-cluster/index>`

.. code-block:: bash

    DYFF_API_FULL_URL=https://api.dyff.local/v0 # Main API
    DYFF_API_AUTH_URL=https://api.dyff.local/auth # Auth API
    DYFF_API_VIEWS_URL=https://api.dyff.local/views # Views API
    DYFF_API_KEY=GENERATED_KEY # For use in tests

Manual Deployment
-----------------

Manual deployment allows for quick development by enabling hot reloading.

Install node dependencies

.. code-block:: bash

    npm i

Run the dev server

.. code-block:: bash

    npm run dev


Open ``localhost:3000`` in your browser

Docker Deployment
-----------------

Hot reloading is not available with the docker deployment.
However, all pages are built at runtime, so navigations are near-instant and represent production deployment load times.

Build the docker image

.. code-block:: bash

    make build

Start the image

.. code-block:: bash

    make run

Styling / Linting
-----------------

For local JS/TS linting, we use ``prettier`` and ``ESLint``

For Gitglab CI linting, the same is performed via `Buildgarden pipelines`_ specified in ``.gitlab-ci.yml``

.. _Buildgarden pipelines: https://gitlab.com/buildgarden

Next.JS Environment Variables
-----------------------------

Environment variables provide the foundation for the "build once deploy anywhere" principle, especially when building and running docker containers.
However, Next.JS dislikes cool things and make it hard on us devs to have runtime environment variables.

Build time environment variables are:

1. Baked into the static generated HTML files for client sided components.
2. Baked into server components / routes if they are not configured as `force-dynamic <https://nextjs.org/docs/app/api-reference/file-conventions/route-segment-config>`_

At runtime, environment variables are:

1. Not available to client-sided components - only build time env variables are exposed since they are baked in
2. Available to server components if they are ``force-dynamic``


There's no officially supported workaround for this except to expose an API route that client sided components can hit and
fetch the server-side environment variable. While this works, it is mega stinky.

See several discussions of this issue `on vercel github issues <https://github.com/vercel/next.js/discussions/44628>`_,

There is `a third party package <https://github.com/expatfile/next-runtime-env>`_
that leverages server component layouts and a React Provider to expose env variables at runtime.
It implements a
`manual solution to the problem <https://github.com/vercel/next.js/discussions/44628#discussioncomment-6918154>`_
for us.
