=============
dyff-frontend
=============

dyff-frontend serves as a visual bridge between the user and dyff-api, facilitating basic CRUD operations and evaluation visualization.

Get started by :doc:`setting up the developer environment <./environment>`.


.. toctree::
    :hidden:

    environment
    types
    testing
