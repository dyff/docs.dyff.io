=====
Types
=====

Given the ``dyff-frontend`` is written in **TypeScript**, and ``dyff-api`` is written in **Python** (using pydantic models for typing), we need to bridge the gap between language types.

`dyff-api` Entity Types
--------------

Generate TypeScript types from the Dyff API openapi.json schema using the ``openapi-typescript`` package:

.. code-block:: bash

    make types

These types are fetched from the following environment variables, and stored in `d.ts` files as follows: 

.. include:: <isonum.txt>

``DYFF_API_FULL_URL`` |rarr| ``types/dyff-api.d.ts``

``DYFF_API_VIEWS_URL`` |rarr| ``types/dyff-views.d.ts``


.env file typing
----------------

Environment variables are set in ``.env``, while their respective types are  defined in ``env.d.ts``
