Kafka
=====

Using Kafka CLI tools
---------------------

.. warning::

    Kafka, like everything in Dyff, is managed via Terraform. Do not change the
    Kafka configuration using the CLI tools.

Create a pod manifest like the following:

.. code-block:: yaml

    apiVersion: v1
    kind: Pod
    metadata:
      name: kafka-cli
      namespace: default
    spec:
      containers:
        - name: shell
          image: jforge/kafka-tools
          command: [ "sleep" ]
          args: [ "99999999" ]

Run it with ``kubectl apply -f``, then login to the pod with:

.. code-block:: console

    $ kubectl exec -it -n default kafka-cli -- bash -il

Once inside the pod, you can use the Kafka CLI tools, e.g.:

.. code-block:: console

    # /opt/kafka/bin/kafka-topics.sh --bootstrap-server kafka.kafka.svc.cluster.local:9092 --describe
