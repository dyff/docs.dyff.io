Tutorial
========

.. toctree::
    :hidden:
    :caption: Tutorial

    datasets
    data-schemas
    inference
    evaluation
    analysis
    safety-cases

This tutorial walks through the core functionality of Dyff.

You will learn how to create a dataset, run inference on the data, anayze the results, and
report on your findings.

Dyff API clients
----------------

You will interact with the Dyff API using a Python client. In the tutorial examples, we will assume that the client instance is called ``dyffapi``. API functions are grouped by the resource type that they affect. For example, to retrieve a :py:class:`~dyff.schema.platform.Dataset` resource, you would use the ``get()`` function for datasets:

.. code-block:: python

    dyffapi.datasets.get("dataset-id")

**We recommend that you complete this tutorial using the** :py:class:`~dyff.audit.local.DyffLocalPlatform` **client.** This implements most of the Dyff API entirely locally within Python. The main things that it *can't* do are:

* Run inference with large AI models that require exotic hardware, and
* Query resources by their attributes (because it doesn't have a database engine).

We provide inference service mock-ups that you can use for prototyping. ``DyffLocalPlatform`` is intended to be API-compatible with the real Dyff API :py:class:`~dyff.client.Client`; in fact, in many tutorial examples, we declare the client instance as:

.. code-block:: python

    dyffapi: Client | DyffLocalPlatform = ...

In most cases, you should be able to swap out the "local" client for the "remote" client and everything will still work.

.. note::

    Use :py:class:`~dyff.audit.local.DyffLocalPlatform` for prototyping and debugging, and swap it for the remote :py:class:`~dyff.client.Client` when everything is ready. This will save you time, give you better error messages, and avoid spending money on cloud computing resources.

Structure of a safety audit
---------------------------

Auditing an AI system consists of running the system on
input data, collecting the outputs, analyzing those outputs, and presenting
conclusions in an audit report.

Dyff structures the audit process into steps called **workflows**.
The following figure shows the data flow through the various workflows ordered
from left to right.

.. Note: Must use HTML 'href' here, as Sphinx :ref: doesn't work in GraphViz.
..       For hrefs in SVG images:
..         * The CWD is 'html/_img', so most hrefs will start with '../'.
..         * 'target' must set; '_top' seems to work.
.. digraph:: dyff_domain_model
    :name: Dyff Domain Model

    node [ordering=in, margin=0];
    newrank=true;
    rankdir=LR;

    "Dataset" [
        label="Dataset",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#dataset",
        target="_top",
    ];

    "InferenceService" [
        label="Inference\nService",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#inference-service",
        target="_top",
    ];

    "InferenceSession" [
        label="Inference\nSession",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#inference-session",
        target="_top",
    ];

    "Evaluation" [
        label="Evaluation",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#evaluation",
        target="_top",
    ];

    "Module" [
        label="Module",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#module",
        target="_top",
    ];

    "Method" [
        label="Method",
        shape="octagon",
        href="../apis/dyff/user-guide/core-resources.html#method",
        target="_top",
    ];

    "Measurement" [
        label="Measurement",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#measurement",
        target="_top",
    ];

    "SafetyCase" [
        label="SafetyCase",
        shape="box",
        href="../apis/dyff/user-guide/core-resources.html#safety-case",
        target="_top",
    ];

    "Model" [
        label="Model",
        shape="box",
        style="dashed",
        href="../apis/dyff/user-guide/core-resources.html#model",
        target="_top",
    ];

    "Score" [
        label="Score",
        shape="box",
        style="dashed",
        href="../apis/dyff/user-guide/core-resources.html#score",
        target="_top",
    ];

    "Dataset" -> "Evaluation";

    "InferenceService" -> "InferenceSession" [label=<<i> 1 ... n </i>>];

    "Evaluation" -> "Method" [label=<<i> 1 ... n </i>>];
    "Module" -> "Method" [label=<<i> n ... 1 </i>>];
    "Method" -> "Measurement";
    "Method" -> "SafetyCase";

    "SafetyCase" -> "Score" [label=<<i> 1 ... n </i>>];

    "Model" -> "InferenceService" [label=<<i> 1 ... n </i>>]

    {
        rank=same;
        edge [style=invis];
        InferenceService -> Dataset;
    }

    {
        rank=same;
        edge [dir=both];
        InferenceSession -> Evaluation;
    }

    {
        rank=same;
        edge [style=invis];
        Module -> Method;
    }

    {
        rank=same;
        edge [style=invis];
        Measurement -> SafetyCase;
    }


.. _dyff-core-resources:

The workflows in the figure correspond to different resource types in the Dyff
Platform. To run a workflow, you use the Dyff API to create instances of the
corresponding resources with appropriate configurations. The resource is stored
in Dyff's database and its status is updated as the work proceeds.

InferenceService
^^^^^^^^^^^^^^^^

An :class:`~dyff.schema.platform.InferenceService` is the "system under test".
Dyff requires that the system is packaged as a Web service that runs in a Docker
container and provides an HTTP API for making inferences on input data. This
generic interface allows Dyff to perform audits directly on
production-ready systems. Dyff can automatically wrap :ref:`models
<core-resources-model>` from popular sources like `Hugging Face`_ as
InferenceServices.

.. _core-resources-model:

Model
^^^^^

A :class:`~dyff.schema.platform.Model` describes the artifacts that comprise an
inference model. Dyff can create InferenceServices automatically for models from
common sources like `Hugging Face`_. In most cases, the model artifacts such
as neural network weights simply get loaded into a "runner" container to expose
the model as an inference service, so services backed by models are cheap to
create.

InferenceSession
^^^^^^^^^^^^^^^^

An :class:`~dyff.schema.platform.InferenceSession` is a running instance of an
InferenceService. Multiple replicas of the service can be run in a single
session to increase throughput. Dyff automatically orchestrates the
computational resources required, including GPU accelerators for neural network
models.

InferenceSessions serve two purposes. First, platform users can use them to
perform inference interactively via the Dyff API. This is useful for experimentation
and prototyping. Second, InferenceSessions are used by :ref:`Evaluations
<core-resources-evaluation>`; the evaluation machinery is implemented as a
"client" of the session that feeds in input data taken from a :ref:`Dataset
<core-resources-dataset>`.

.. _core-resources-dataset:

Dataset
^^^^^^^

A :class:`~dyff.schema.platform.Dataset` is a set of input instances on which to
evaluate systems. Dyff uses the `Apache Arrow`_ format to represent datasets.
The Arrow format is mostly inter-convertible with JSON and Pandas
DataFrame formats. An Arrow dataset has a static schema describing the names and
types of columns. Dyff includes various :doc:`schemas <data-schemas>` for common
types of data.

.. _core-resources-evaluation:

Evaluation
^^^^^^^^^^

An :class:`~dyff.schema.platform.Evaluation` is the process of making an
inference for each instance in a dataset using a given inference service -- for
example, classifying all of the images in ImageNet using a particular neural
network model. The result of an evaluation is another Apache Arrow dataset
containing the inference outputs.

.. _core-resources-method:

Method
^^^^^^

A :class:`~dyff.schema.platform.Method` is an implementation of an analysis process. Methods take inputs -- either :ref:`Evaluation
<core-resources-evaluation>` outputs or the outputs of other intermediate Methods -- and produce either :ref:`Measurements
<core-resources-measurement>` or :ref:`SafetyCases
<core-resources-safetycase>`. Any number of different applicable methods can be run against a single set of evaluation outputs.

.. _core-resources-module:

Module
^^^^^^

A :class:`~dyff.schema.platform.Module` contains Python code artifacts that can be used by :ref:`Methods <core-resources-method>`. At runtime, the root directoy of the Module will be mounted at a known location and added to the ``PYTHONPATH`` so that it can be imported. Methods can use multiple modules.

.. _core-resources-measurement:

Measurement
^^^^^^^^^^^

A :class:`~dyff.schema.platform.Measurement` is new Arrow dataset resulting from
applying a :class:`~dyff.schema.platform.Method` to one or more input datasets
-- either the outputs of Evaluations, or other Measurements. For a simple
classification task, for example, the method might assign a 0-1 score to each
instance according to whether the correct label was among the top-:math:`k`
highest-scoring predicted labels.

One important use case for Measurements is to post-process Evaluation outputs to
anonymize them or remove sensitive information. The result can then be shared
publicly, allowing others to analyze it.

.. note::

    In previous Dyff versions, the :class:`~dyff.schema.platform.Report`
    resource provided similar functionality to the combination of ``Method`` +
    ``Measurement``, but with less flexibility. ``Report`` is deprecated and
    will be removed in a future version.

.. _core-resources-safetycase:

SafetyCase
^^^^^^^^^^^

A :class:`~dyff.schema.platform.SafetyCase` is an artifact suitable for
human consumption that presents the overall evidence for the safety or un-safety of a system for a particular use case.

Like measurements, safety cases are generated by running a :py:class:`Methods <dyff.schema.platform.Method>`, but the methods that produce safety cases are
implemented as Jupyter notebooks. The notebook generates an output document
consisting of formatted text, figures, tables, and other multi-media content.
The Dyff Platform, using Jupyter tools, renders the output documents as HTML
pages and serves them on the Web.

.. _core-resources-score:

Score
^^^^^

In addition to the main safety case document, :ref:`SafetyCases <core-resources-safetycase>` may also "export" one or more scalar-valued :class:`Scores <dyff.schema.platform.Score>`. Scores are used by the :doc:`Dyff App <../cloud/integration>` to populate various performance summary and comparison views.


.. Links
.. _Apache Arrow: https://arrow.apache.org/
.. _Hugging Face: https://huggingface.co/
