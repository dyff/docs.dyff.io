Create a Dataset
================

In this section, we will create our first Dyff resource: the Dataset.

Create an Arrow dataset locally
-------------------------------

Dyff uses the `Apache Arrow <https://arrow.apache.org/>`_ format for storing
datasets. To upload a new dataset, you first need to create an Arrow dataset
locally. Arrow is a column-oriented format that's mostly inter-convertible with
Pandas DataFrames and JSON. In this guide, we're going to create a simple text
dataset, which will have one data column called ``text``. Arrow is strongly
typed, and all Arrow datasets must have an associated schema.

Define some example text data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For this guide, we're going to create a simple text dataset containing the
following data consisting of opening phrases from some famous novels:

.. literalinclude:: examples/dataset.py
    :language: python
    :lines: 13-20
    :lineno-start: 13

Define the dataset schema
~~~~~~~~~~~~~~~~~~~~~~~~~

We'll use some utilities from the Dyff client library to create the necessary
schema:

.. literalinclude:: examples/dataset.py
    :language: python
    :lines: 21-23
    :lineno-start: 21

Dyff expects datasets to have certain additional "metadata" columns and
structure. The required structure is described in detail in the :doc:`Data
Schemas <data-schemas>` guide. For now, you just need to know that input
datasets must have an integer column called ``_index_`` that uniquely identifies
each row in the dataset. The ``DataSchema.make_input_schema()`` function creates
a schema with the required structure for input datasets. The argument is the
schema of the data; in this case, that's ``dyff.schema.dataset.text.Text``,
which has a single field ``text`` of type ``str``. After creating the schema, we
need to "decode" the Arrow schema because it's a Python object that's encoded in
binary in the ``DataSchema`` object.

Generate the dataset
~~~~~~~~~~~~~~~~~~~~

Now we define the data generating function. To write an Arrow dataset to disk,
we need to define an ``Iterable`` of ``pyarrow.RecordBatch`` objects. The record
batches can be constructed in multiple ways. Usually, the easiest way is from
what PyArrow calls the ``pylist`` format, which is a list of ``dict`` objects,
each of which represents one "row" in the dataset. When generating the data, we
need to add the metadata fields required by Dyff; in this case, that's the
``_index_`` field:

.. literalinclude:: examples/dataset.py
    :language: python
    :lines: 25-28
    :lineno-start: 25

Write the dataset to disk
~~~~~~~~~~~~~~~~~~~~~~~~~

Now we need to write the dataset in Arrow format. When interacting with Dyff
Platform datasets, always use the functions in ``dyff.schema.dataset.arrow``
rather than the corresponding ``pyarrow`` functions, because the ones in the
Dyff client library set some necessary default parameters. We'll use a temporary
directory to hold the files.

.. literalinclude:: examples/dataset.py
    :language: python
    :lines: 30-42
    :lineno-start: 30
    :emphasize-lines: 1-5

Upload the dataset
~~~~~~~~~~~~~~~~~~

Uploading the dataset happens in two steps. First, you create a ``Dataset``
record. Dyff assigns a unique ID to the dataset and returns a full ``Dataset``
object. Second, you upload the actual data, providing the ``Dataset`` object so
that Dyff knows where to store the data. The hashes of the dataset files must
match the hashes calculated in the ``create`` step.

.. literalinclude:: examples/dataset.py
    :language: python
    :lines: 30-42
    :lineno-start: 30
    :emphasize-lines: 6-13

Full example
------------

.. literalinclude:: examples/dataset.py
    :language: python
    :linenos:
