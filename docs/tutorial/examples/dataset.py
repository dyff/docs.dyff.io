import tempfile

import pyarrow

from dyff.audit.local import DyffLocalPlatform
from dyff.client import Client
from dyff.schema.dataset import arrow, text
from dyff.schema.platform import DataSchema

dyffapi: Client | DyffLocalPlatform = ...
ACCOUNT: str = ...

DATA = [
    "Call me Ishmael. ",
    "It was the best of times, ",
    "A screaming comes across the sky. ",
    "It was a bright cold day in April, ",
    "In a hole in the ground there lived a hobbit. ",
]

schema = DataSchema.make_input_schema(text.Text)
arrow_schema = arrow.decode_schema(schema.arrowSchema)


def data_generator():
    pylist = [{"_index_": i, "text": t} for i, t in enumerate(DATA)]
    yield pyarrow.RecordBatch.from_pylist(pylist, schema=arrow_schema)


with tempfile.TemporaryDirectory() as tmpdir:
    arrow.write_dataset(
        data_generator(), output_path=tmpdir, feature_schema=arrow_schema
    )

    dataset = dyffapi.datasets.create_arrow_dataset(
        tmpdir,
        account=ACCOUNT,
        name="famous-first-phrases",
    )
    dyffapi.datasets.upload_arrow_dataset(dataset, tmpdir)

    print(dataset.json(indent=2))
