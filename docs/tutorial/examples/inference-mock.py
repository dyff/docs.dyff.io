from dyff.audit.local import DyffLocalPlatform, mocks
from dyff.schema.requests import InferenceSessionCreateRequest

dyffapi = DyffLocalPlatform(storage_root="/some/dir")
ACCOUNT = ...

service = dyffapi.inferenceservices.create_mock(mocks.TextCompletion, account=ACCOUNT)
print(service.json(indent=2))

session_request = InferenceSessionCreateRequest(
    account=ACCOUNT, inferenceService=service.id
)
session_and_token = dyffapi.inferencesessions.create(session_request)
session = session_and_token.inferencesession
token = session_and_token.session
print(session.json(indent=2))

inference_client = dyffapi.inferencesessions.client(
    session.id, token, interface=session.inferenceService.interface
)

completion = inference_client.infer({"text": "Open the pod bay doors, Hal!"})
print(completion)
