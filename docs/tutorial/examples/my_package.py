# mypy: disable-error-code="import-untyped"
import re
import statistics
from typing import Iterable

import pandas
import pyarrow
import pydantic

from dyff.schema.dataset import ReplicatedItem
from dyff.schema.dataset.arrow import arrow_schema


class WordLengthScoredItem(ReplicatedItem):
    meanPromptWordLength: float = pydantic.Field(
        description="Mean number of characters in the words in the prompt text."
    )
    meanCompletionWordLength: float = pydantic.Field(
        description="Mean number of characters in the words in the system completions."
    )
    wordOfTheDay: str = pydantic.Field(
        description="The value of the 'wordOfTheDay' argument to the Method."
    )


def word_length(
    args: dict[str, str],
    *,
    prompts: pyarrow.dataset.Dataset,
    completions: pyarrow.dataset.Dataset,
) -> Iterable[pyarrow.RecordBatch]:
    schema = arrow_schema(WordLengthScoredItem)
    prompts_df: pandas.DataFrame = prompts.to_table().to_pandas()
    completions_df: pandas.DataFrame = completions.to_table().to_pandas()

    def _mean_word_length(text: str) -> float:
        words = re.split(r"\s", text.strip())
        if len(words) == 0:
            return 0.0
        else:
            return statistics.mean(len(word) for word in words)

    prompts_df["meanPromptWordLength"] = prompts_df.apply(
        lambda row: _mean_word_length(row["text"]), axis=1
    ).drop("text")
    completions_df["meanCompletionWordLength"] = completions_df.apply(
        lambda row: _mean_word_length(row["responses"][0]["text"]), axis=1
    ).drop("responses")

    measurement_df = prompts_df.merge(completions_df, on="_index_")
    measurement_df["wordOfTheDay"] = args["wordOfTheDay"]

    yield from pyarrow.Table.from_pandas(measurement_df, schema=schema).to_batches()
