# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

from pathlib import Path

from dyff.audit.local import DyffLocalPlatform
from dyff.schema.platform import *
from dyff.schema.requests import *

ACCOUNT: str = ...
ROOT_DIR: Path = Path("/home/me/dyff")

# Develop using the local platform
dyffapi = DyffLocalPlatform(
    storage_root=ROOT_DIR / ".dyff-local",
)
# When you're ready, switch to the remote platform:
# dyffapi = Client(...)

module_root = str(ROOT_DIR / "my-notebook")
module = dyffapi.modules.create_package(
    module_root,
    account=ACCOUNT,
    name="my-notebook",
)
dyffapi.modules.upload_package(module, module_root)
print(module.json(indent=2))

method_request = MethodCreateRequest(
    name="mean-word-length-notebook",
    # The notebook analyzes multiple measurements of the same system
    scope=MethodScope.InferenceService,
    description="Visualizes the mean word length in prompts and system completions.",
    # The method is implemented as a Jupyter notebook
    implementation=MethodImplementation(
        kind=MethodImplementationKind.JupyterNotebook,
        jupyterNotebook=MethodImplementationJupyterNotebook(
            notebookModule=module.id,
            # The path to the notebook file, relative to the module root directory
            notebookPath="my-notebook.ipynb",
        ),
    ),
    # The method accepts one argument called 'threshold'
    parameters=[
        MethodParameter(keyword="threshold", description="(float) A numeric threshold"),
    ],
    # The method accepts two PyArrow datasets as inputs:
    # - The one called 'easy' is a Measurement on the "easy" dataset
    # - The one called 'hard' is a Measurement on the "hard" dataset
    inputs=[
        MethodInput(kind=MethodInputKind.Measurement, keyword="easy"),
        MethodInput(kind=MethodInputKind.Measurement, keyword="hard"),
    ],
    # The method produces a SafetyCase
    output=MethodOutput(
        kind=MethodOutputKind.SafetyCase,
        safetyCase=SafetyCaseSpec(
            name="mean-word-length-safetycase",
            description="Visualizes the mean word length in prompts and system completions.",
        ),
    ),
    # The Module containing the notebook code
    modules=[module.id],
    account=ACCOUNT,
)
method = dyffapi.methods.create(method_request)
print(method.json(indent=2))

easy_measurement_id: str = ...
hard_measurement_id: str = ...
analysis_request = AnalysisCreateRequest(
    account=ACCOUNT,
    method=method.id,
    arguments=[
        AnalysisArgument(keyword="threshold", value="1.0"),
    ],
    inputs=[
        AnalysisInput(keyword="easy", entity=easy_measurement_id),
        AnalysisInput(keyword="hard", entity=hard_measurement_id),
    ],
)
safetycase = dyffapi.safetycases.create(analysis_request)
print(safetycase.json(indent=2))
