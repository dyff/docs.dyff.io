Run an Evaluation
=================

In Dyff, we call the process of running input data through an AI/ML system to produce outputs an ``Evaluation``. Now that we've create a dataset, the next step is to run (or simulate) an Evaluation on that dataset.

Run an evaluation on the platform
---------------------------------

The process of running an evaluation is the same whether you're using a ``Client`` connnected to a Dyff deployment, or a ``DyffLocalPlatform`` instance running locally.

.. note::

    Some of the evaluation parameters that control hardware configurations have no effect when using a ``DyffLocalPlatform`` instance.

.. code-block:: python
    :linenos:

    from datetime import datetime, timedelta
    from dyff.schema.requests import (
        EvaluationCreateRequest,
        EvaluationInferenceSessionRequest,
    )

    dyffapi = ...
    dataset_id: str = ...
    service_id: str = ...
    evaluation_request = EvaluationCreateRequest(
        account=account,
        dataset=dataset_id,
        inferenceSession=EvaluationInferenceSessionRequest(
            inferenceService=service_id,
        ),
        replications=2,
    )
    evaluation = dyffapi.evaluations.create(evaluation_request)
    print(evaluation.json(indent=2))

This request will run the evaluation with 2 replications, meaning that inference will be run on the entire dataset twice. This capability is useful with non-deterministic inference models to create a distribution over outputs.

When running on a Dyff deployment, the ``create()`` call will return immediately. You can monitor progress using ``dyffapi.evaluations.get(evaluation.id).status``. The ``.status`` will be ``Complete`` when the evaluation is finished.

Run local data through a remote session
---------------------------------------

You can also run an evaluation on local data
using a remote inference session. This capability requires that you provide a
:class:`~dyff.client.Client` instance that can communicate with an appropriate
remotely-hosted Dyff instance:

.. literalinclude:: examples/local-evaluation.py
    :language: python
    :linenos:
    :emphasize-lines: 9-10

Then, you can run a local dataset managed by the
:class:`~dyff.audit.local.DyffLocalPlatform` through the remote inference
session:

.. literalinclude:: examples/local-evaluation.py
    :language: python
    :linenos:
    :emphasize-lines: 17-20
