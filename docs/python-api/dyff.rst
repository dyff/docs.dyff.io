dyff
====

The ``dyff`` Python package is a "meta-package" that pulls in other Dyff
packages. It provides no functionality of its own.

At the time of writing, installing ``dyff`` will install :mod:`dyff.audit`,
:mod:`dyff.client`, and :mod:`dyff.schema`.

After installing this way, you can upgrade ``dyff`` to get the latest full SDK release, or you can upgrade individual packages:

.. code-block:: bash

    python3 -m pip install --upgrade dyff
    python3 -m pip install --upgrade dyff-audit
