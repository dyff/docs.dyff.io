Utilities
=========

Scoring Rubrics
---------------

.. automodule:: dyff.audit.scoring.base
   :members:
   :no-show-inheritance:

Classification
~~~~~~~~~~~~~~

.. automodule:: dyff.audit.scoring.classification
   :members:

Text
~~~~

.. automodule:: dyff.audit.scoring.text
   :members:

Evaluation Metrics
------------------

.. automodule:: dyff.audit.metrics
   :members:
   :no-inherited-members:
   :special-members: __call__

Text
~~~~

.. automodule:: dyff.audit.metrics.text
   :members:
   :exclude-members: Metric, DyffSchemaBaseModel
   :special-members: __call__

Dataset Construction and Manipulation
-------------------------------------

.. automodule:: dyff.audit.data
   :members:
   :no-inherited-members:

Text
~~~~

.. automodule:: dyff.audit.data.text
   :members:
   :no-inherited-members:

Workflows
---------

.. automodule:: dyff.audit.workflows
   :members:
   :no-show-inheritance:
