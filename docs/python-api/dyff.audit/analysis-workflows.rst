.. Cut the right ("Contents") sidebar at the "class name" level

:tocdepth: 3

Analysis workflows
==================

The ``dyff.audit.analysis`` module contains tools for implementing analysis :py:class:`Methods <dyff.schema.platform.Method>` and running them on platform data. The Dyff platform uses the functions in ``dyff.audit.analysis.runners`` to run the Method code, and you can use these same functions via the :py:class:`~dyff.audit.local.DyffLocalPlatform` to develop and debug analysis code locally.

Within a Method implementation, you use :py:class:`~dyff.audit.analysis.AnalysisContext` to access inputs and metadata for the current analysis, display formatted conclusions in Jupyter notebooks, and output artifacts such as :py:class:`Scores <dyff.schema.platform.Score>` to be stored by Dyff. See the :doc:`SafetyCase tutorial <../../tutorial/safety-cases>` for usage examples.

Analysis context
----------------

.. autoclass:: dyff.audit.analysis.AnalysisContext
    :no-show-inheritance:
    :class-doc-from: both

Runners
-------

.. autofunction:: dyff.audit.analysis.runners.run_analysis

.. autofunction:: dyff.audit.analysis.runners.run_report
