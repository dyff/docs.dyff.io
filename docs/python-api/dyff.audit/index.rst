dyff.audit
==========

.. module:: dyff.audit

.. toctree::
   :hidden:

   analysis-workflows
   local-platform
   utilities

The `dyff.audit <https://pypi.org/project/dyff-audit>`_ package provides tools for
producing audit reports from the raw outputs of intelligent systems.

Installation
------------

.. code-block:: bash

   python3 -m pip install dyff-audit
