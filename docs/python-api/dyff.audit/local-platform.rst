.. Cut the right ("Contents") sidebar at the "class name" level

:tocdepth: 3

Local development
=================

The ``dyff.audit.local`` module contains tools for developing analyses locally without needing to interact with a full Dyff platform instance.

The primary interface for local development is the :py:class:`~dyff.audit.local.DyffLocalPlatform` class. The local platform implements most of the Dyff API functionality exposed by the :py:class:`Dyff Client <dyff.client.Client>` with the same interface but running entirely within Python.

There are also mock-ups for inference functionality in ``dyff.audit.local.mocks``. These can be used to simulate running an :py:class:`~dyff.schema.platform.InferenceService` without the overhead of running an actual machine learning model.

Local platform client
---------------------

.. autoclass:: dyff.audit.local.DyffLocalPlatform
    :no-show-inheritance:
    :members:
    :no-inherited-members:
    :class-doc-from: both

.. autoclass:: dyff.audit.local.mocks.InferenceSessionClientMock
    :no-show-inheritance:
    :members:
    :no-inherited-members:
    :class-doc-from: both


API groups
----------

.. autoclass:: dyff.audit.local.platform._Datasets()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Dataset]
    .. automethod:: fetch(id: str) -> Dataset


.. autoclass:: dyff.audit.local.platform._Evaluations()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Evaluation]
    .. automethod:: fetch(id: str) -> Evaluation


.. autoclass:: dyff.audit.local.platform._InferenceServices()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[InferenceService]
    .. automethod:: fetch(id: str) -> InferenceService


.. autoclass:: dyff.audit.local.platform._InferenceSessions()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[InferenceSession]
    .. automethod:: fetch(id: str) -> InferenceSession


.. autoclass:: dyff.audit.local.platform._Measurements()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Measurement]
    .. automethod:: fetch(id: str) -> Measurement


.. autoclass:: dyff.audit.local.platform._Methods()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Method]
    .. automethod:: fetch(id: str) -> Method


.. autoclass:: dyff.audit.local.platform._Models()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Model]
    .. automethod:: fetch(id: str) -> Model


.. autoclass:: dyff.audit.local.platform._Modules()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Module]
    .. automethod:: fetch(id: str) -> Module


.. autoclass:: dyff.audit.local.platform._Reports()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[Report]
    .. automethod:: fetch(id: str) -> Report


.. autoclass:: dyff.audit.local.platform._SafetyCases()
    :no-show-inheritance:
    :members:
    :inherited-members:
    :exclude-members: get, fetch

    .. automethod:: get(id: str) -> Optional[SafetyCase]
    .. automethod:: fetch(id: str) -> SafetyCase


.. autoclass:: dyff.audit.local.platform._Scores()
    :no-show-inheritance:
    :members:
    :inherited-members:

InferenceService mocks
----------------------

.. autoclass:: dyff.audit.local.mocks.InferenceServiceMock
    :members:

.. autoclass:: dyff.audit.local.mocks.TextCompletion
    :members:
