.. Cut the right ("Contents") sidebar at the "class name" level

:tocdepth: 3

dyff.client
===========

.. module:: dyff.client
   :synopsis: Client for interacting with Dyff.

The `dyff.client <https://pypi.org/project/dyff-client>`_ package provides a
Python client for interacting with the Dyff REST API.

Installation
------------

Install the ``dyff-client`` package from PyPI:

.. code-block:: bash

    python3 -m pip install dyff-client

Configuration
-------------

Here are well-known API locations:

- ``http://api.dyff.local/v0`` - A self-hosted Dyff on a local machine.

- ``https://api.dyff.io/v0`` - The Dyff instance hosted by UL DSRI.

Conventions
-----------

API functions are grouped by the resource type that they manipulate, for example, :py:meth:`Client.datasets.create <dyff.client._apigroups._Datasets.create>` will create a :py:class:`~dyff.schema.platform.Dataset` resource. Most resources support most of the same basic operations, and the corresponding API functions will have similar signatures across different resources. The operations on :py:class:`~dyff.schema.platform.Dataset` resources are defined in :py:class:`~dyff.client._apigroups._Datasets`.

Many API functions accept a ``Request`` object that holds the data to pass to the function. Refer to the :py:mod:`dyff.schema.requests` documentation for details.

Client reference
----------------

.. Note that we have to override the signatures for methods that are inherited
.. from a generic mixin class because sphinx generates a signature with the
.. generic type parameter rather than the actual concrete type

.. autoclass:: dyff.client.Client
   :no-show-inheritance:
   :members:
   :no-inherited-members:
   :class-doc-from: both

.. autoclass:: dyff.client.InferenceSessionClient()
   :no-show-inheritance:
   :members:
   :no-inherited-members:
   :class-doc-from: both

API groups
----------

.. autoclass:: dyff.client._apigroups._Datasets()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: DatasetCreateRequest | dict[str, Any]) -> Dataset
   .. automethod:: get(id: str) -> Dataset

.. autoclass:: dyff.client._apigroups._Evaluations()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: EvaluationCreateRequest | dict[str, Any]) -> Evaluation
   .. automethod:: get(id: str) -> Evaluation

.. autoclass:: dyff.client._apigroups._InferenceServices()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: InferenceServiceCreateRequest | dict[str, Any]) -> InferenceService
   .. automethod:: get(id: str) -> InferenceService

.. autoclass:: dyff.client._apigroups._InferenceSessions()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: InferenceSessionCreateRequest | dict[str, Any]) -> InferenceSession
   .. automethod:: get(id: str) -> InferenceSession

.. autoclass:: dyff.client._apigroups._Measurements()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: AnalysisCreateRequest | dict[str, Any]) -> Measurement
   .. automethod:: get(id: str) -> Measurement

.. autoclass:: dyff.client._apigroups._Methods()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: MethodCreateRequest | dict[str, Any]) -> Method
   .. automethod:: get(id: str) -> Method

.. autoclass:: dyff.client._apigroups._Models()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: ModelCreateRequest | dict[str, Any]) -> Model
   .. automethod:: get(id: str) -> Model

.. autoclass:: dyff.client._apigroups._Modules()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: ModuleCreateRequest | dict[str, Any]) -> Module
   .. automethod:: get(id: str) -> Module

.. autoclass:: dyff.client._apigroups._Reports()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: ReportCreateRequest | dict[str, Any]) -> Report
   .. automethod:: get(id: str) -> Report

.. autoclass:: dyff.client._apigroups._SafetyCases()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: AnalysisCreateRequest | dict[str, Any]) -> SafetyCase
   .. automethod:: get(id: str) -> SafetyCase

.. autoclass:: dyff.client._apigroups._UseCases()
   :no-show-inheritance:
   :members:
   :inherited-members:
   :exclude-members: create, get

   .. automethod:: create(request: ConcernCreateRequest | dict[str, Any]) -> UseCase
   .. automethod:: get(id: str) -> UseCase
