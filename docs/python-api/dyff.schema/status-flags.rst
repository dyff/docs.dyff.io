Status flags
============

.. autofunction:: dyff.schema.platform.is_status_failure

.. autofunction:: dyff.schema.platform.is_status_success

.. autofunction:: dyff.schema.platform.is_status_terminal

.. autodata:: dyff.schema.platform.JobStatus

.. autodata:: dyff.schema.platform.EntityStatus

.. autodata:: dyff.schema.platform.EntityStatusReason

.. autodata:: dyff.schema.platform.DataSourceStatus

.. autodata:: dyff.schema.platform.DatasetStatus

.. autodata:: dyff.schema.platform.DatasetStatusReason

.. autodata:: dyff.schema.platform.EvaluationStatus

.. autodata:: dyff.schema.platform.EvaluationStatusReason

.. autodata:: dyff.schema.platform.InferenceServiceStatus

.. autodata:: dyff.schema.platform.InferenceServiceStatusReason

.. autodata:: dyff.schema.platform.ModelStatus

.. autodata:: dyff.schema.platform.ModelStatusReason

.. autodata:: dyff.schema.platform.ReportStatus

.. autodata:: dyff.schema.platform.ReportStatusReason
