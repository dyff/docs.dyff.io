dyff.schema
===========

.. module:: dyff.schema

.. toctree::
    :hidden:

    platform-types
    request-types
    status-flags
    schema-adapters
    utilities

The `dyff.schema <https://pypi.org/project/dyff-schema>`_  package contains schema definitions for Dyff resources.

Installation
------------

Install the ``dyff-schema`` package from PyPI:

.. code-block:: bash

    python3 -m pip install dyff-schema

Pydantic models
---------------

Dyff uses `pydantic models <https://docs.pydantic.dev/1.10/>`_ to
formalize all of its data schemas. We
also use the models to generate schemas in the JSON Schema format, which, in
turn, form part of the OpenAPI specification of the Dyff API.

.. note::

    Dyff currently uses Pydantic v1. A migration to v2 is planned.

Pydantic models are easy to convert to and from JSON, and they perform validation when loading JSON data. To create a model instance from JSON data, use ``ModelType.parse_obj()`` or ``ModelType.parse_raw()``. To convert a model instance to a JSON string, use ``model.json()``. Use ``model.dict()`` to convert the model to a (nested) Python ``dict``. Note that the result of ``.dict()`` may contain data that is not of a JSON native type (such as ``datetime`` objects). If you need a Python ``dict`` containing only JSON native types, you can use ``model.model_dump(mode="json")``. This is a Pydantic 2 API function that we have back-ported to Dyff schema models. It uses an inefficient mechanism to do the type conversions, so it should be used sparingly. The ``.dict()`` function is equivalent to ``.model_dump(mode="python")``, which is the default mode.

Note that in some cases, the
"canonical name" of a field is not valid as a pydantic model field name, either
because it conflicts with fields inherited from ``pydantic.BaseModel``, or
because it is a Python reserved word like ``bytes``. In these cases, the field
will have an ``alias`` set to its canonical name. All Dyff schema models
derive from :class:`~dyff.schema.platform.DyffBaseModel`,
which overrides ``dict()`` and ``json()`` so that they default to using the
aliases as the keys when building JSON objects. You generally should *not*
override this behavior.
