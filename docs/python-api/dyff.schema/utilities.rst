Utilities
=========

.. autofunction:: dyff.schema.product_schema

.. autofunction:: dyff.schema.platform.entity_class

.. autofunction:: dyff.schema.platform.identifier_regex

.. autofunction:: dyff.schema.platform.identifier_maxlen

.. autofunction:: dyff.schema.platform.summary_maxlen

.. autofunction:: dyff.schema.platform.title_maxlen
