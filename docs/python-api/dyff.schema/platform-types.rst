.. Cut the right ("Contents") sidebar at the "class name" level

:tocdepth: 3

.. py:module:: dyff.schema.platform

Platform types
==============

The Pydantic models in :py:mod:`dyff.schema.platform` define the data schema for entities stored in the Dyff system. You will typically encounter these types in the responses from Dyff API functions.

Core platform types
-------------------

The core platform types are the ones that you create, manage, and query through
the Dyff API. The core types describe the steps of the auditing workflow that
produces audit reports from models and data. Instances of core types all have a
unique ``.id``, belong to an ``.account``, and have additional metadata fields
that are updated by the platform. In particular, the ``.status`` and ``.reason``
fields tell you how the work is proceeding and whether it is complete or
encountered an error.

.. autopydantic_model:: dyff.schema.platform.Dataset
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Evaluation
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.InferenceService
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.InferenceSession
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Measurement
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Method
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Model
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Module
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Report
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.SafetyCase
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.Score
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.platform.UseCase
    :inherited-members: BaseModel, DyffBaseModel


Additional Platform Types
-------------------------

.. autopydantic_model:: dyff.schema.platform.Accelerator

.. autopydantic_model:: dyff.schema.platform.AcceleratorGPU

.. autopydantic_model:: dyff.schema.platform.AccessGrant

.. autopydantic_model:: dyff.schema.platform.Account

.. autopydantic_model:: dyff.schema.platform.Analysis

.. autopydantic_model:: dyff.schema.platform.AnalysisArgument

.. autopydantic_model:: dyff.schema.platform.AnalysisBase

.. autopydantic_model:: dyff.schema.platform.AnalysisInput

.. autopydantic_model:: dyff.schema.platform.AnalysisOutputQueryFields

.. autopydantic_model:: dyff.schema.platform.AnalysisScope

.. autopydantic_model:: dyff.schema.platform.Annotation

.. autoclass:: dyff.schema.platform.APIFunctions

.. autopydantic_model:: dyff.schema.platform.APIKey

.. autopydantic_model:: dyff.schema.platform.ArchiveFormat

.. autopydantic_model:: dyff.schema.platform.Artifact

.. autopydantic_model:: dyff.schema.platform.ArtifactURL

.. autopydantic_model:: dyff.schema.platform.Audit

.. autopydantic_model:: dyff.schema.platform.AuditProcedure

.. autopydantic_model:: dyff.schema.platform.AuditRequirement

.. autopydantic_model:: dyff.schema.platform.Concern

.. autopydantic_model:: dyff.schema.platform.ConcernBase

.. autopydantic_model:: dyff.schema.platform.ContainerImageSource

.. autopydantic_model:: dyff.schema.platform.DataSchema

.. autopydantic_model:: dyff.schema.platform.DataSource

.. autoattribute:: dyff.schema.platform.DataSources

.. autopydantic_model:: dyff.schema.platform.DataView

.. autopydantic_model:: dyff.schema.platform.DatasetBase

.. autopydantic_model:: dyff.schema.platform.DatasetFilter

.. autopydantic_model:: dyff.schema.platform.Digest

.. autopydantic_model:: dyff.schema.platform.Documentation

.. autopydantic_model:: dyff.schema.platform.DocumentationBase

.. autopydantic_model:: dyff.schema.platform.Documented

.. autopydantic_model:: dyff.schema.platform.DyffDataSchema

.. autopydantic_model:: dyff.schema.platform.DyffEntity

.. :py:attribute:: dyff.schema.platform.DyffEntityType

.. autopydantic_model:: dyff.schema.platform.DyffModelWithID

.. autopydantic_model:: dyff.schema.platform.DyffSchemaBaseModel

.. autoclass:: dyff.schema.platform.Entities

.. autoattribute:: dyff.schema.platform.EntityID

.. autopydantic_model:: dyff.schema.platform.EvaluationBase

.. autopydantic_model:: dyff.schema.platform.ExtractorStep

.. autopydantic_model:: dyff.schema.platform.Family

.. autopydantic_model:: dyff.schema.platform.FamilyBase

.. autopydantic_model:: dyff.schema.platform.FamilyMember

.. autopydantic_model:: dyff.schema.platform.FamilyMemberBase

.. autoclass:: dyff.schema.platform.FamilyMemberKind

.. autopydantic_model:: dyff.schema.platform.FamilyMembers

.. autopydantic_model:: dyff.schema.platform.ForeignInferenceService

.. autopydantic_model:: dyff.schema.platform.ForeignMethod

.. autopydantic_model:: dyff.schema.platform.ForeignModel

.. autoclass:: dyff.schema.platform.Frameworks

.. autopydantic_model:: dyff.schema.platform.Hazard

.. autopydantic_model:: dyff.schema.platform.History

.. autopydantic_model:: dyff.schema.platform.Identity

.. autopydantic_model:: dyff.schema.platform.InferenceInterface

.. autopydantic_model:: dyff.schema.platform.InferenceServiceBase

.. autopydantic_model:: dyff.schema.platform.InferenceServiceBuilder

.. autopydantic_model:: dyff.schema.platform.InferenceServiceRunner

.. autoclass:: dyff.schema.platform.InferenceServiceRunnerKind

.. autoclass:: dyff.schema.platform.InferenceServiceSources

.. autopydantic_model:: dyff.schema.platform.InferenceServiceSpec

.. autopydantic_model:: dyff.schema.platform.InferenceSessionAndToken

.. autopydantic_model:: dyff.schema.platform.InferenceSessionBase

.. autopydantic_model:: dyff.schema.platform.InferenceSessionReference

.. autopydantic_model:: dyff.schema.platform.InferenceSessionSpec

.. autopydantic_model:: dyff.schema.platform.Label

.. :py:attribute:: dyff.schema.platform.LabelKey

.. :py:attribute:: dyff.schema.platform.LabelValue

.. autopydantic_model:: dyff.schema.platform.Labeled

.. autoclass:: dyff.schema.platform.MeasurementLevel

.. autopydantic_model:: dyff.schema.platform.MeasurementSpec

.. autopydantic_model:: dyff.schema.platform.MethodBase

.. autopydantic_model:: dyff.schema.platform.MethodImplementation

.. autopydantic_model:: dyff.schema.platform.MethodImplementationJupyterNotebook

.. autoclass:: dyff.schema.platform.MethodImplementationKind

.. autopydantic_model:: dyff.schema.platform.MethodImplementationPythonFunction

.. autopydantic_model:: dyff.schema.platform.MethodImplementationPythonRubric

.. autopydantic_model:: dyff.schema.platform.MethodInput

.. autoclass:: dyff.schema.platform.MethodInputKind

.. autopydantic_model:: dyff.schema.platform.MethodOutput

.. autoclass:: dyff.schema.platform.MethodOutputKind

.. autopydantic_model:: dyff.schema.platform.MethodParameter

.. autoclass:: dyff.schema.platform.MethodScope

.. autopydantic_model:: dyff.schema.platform.ModelArtifact

.. autopydantic_model:: dyff.schema.platform.ModelArtifactHuggingFaceCache

.. autoclass:: dyff.schema.platform.ModelArtifactKind

.. autopydantic_model:: dyff.schema.platform.ModelBase

.. autopydantic_model:: dyff.schema.platform.ModelResources

.. autopydantic_model:: dyff.schema.platform.ModelSource

.. autopydantic_model:: dyff.schema.platform.ModelSourceGitLFS

.. autopydantic_model:: dyff.schema.platform.ModelSourceHuggingFaceHub

.. autoclass:: dyff.schema.platform.ModelSourceKinds

.. autopydantic_model:: dyff.schema.platform.ModelSourceOpenLLM

.. autopydantic_model:: dyff.schema.platform.ModelSpec

.. autopydantic_model:: dyff.schema.platform.ModelStorage

.. autoclass:: dyff.schema.platform.ModelStorageMedium

.. autopydantic_model:: dyff.schema.platform.ModuleBase

.. autopydantic_model:: dyff.schema.platform.QueryableDyffEntity

.. autopydantic_model:: dyff.schema.platform.ReportBase

.. autoclass:: dyff.schema.platform.Resources

.. autopydantic_model:: dyff.schema.platform.Revision

.. autopydantic_model:: dyff.schema.platform.RevisionMetadata

.. autopydantic_model:: dyff.schema.platform.Role

.. autopydantic_model:: dyff.schema.platform.SafetyCaseSpec

.. autopydantic_model:: dyff.schema.platform.SchemaAdapter

.. autopydantic_model:: dyff.schema.platform.ScoreData

.. autopydantic_model:: dyff.schema.platform.ScoreMetadata

.. autopydantic_model:: dyff.schema.platform.ScoreMetadataRefs

.. autopydantic_model:: dyff.schema.platform.ScoreSpec

.. autopydantic_model:: dyff.schema.platform.Status

.. autopydantic_model:: dyff.schema.platform.StorageSignedURL

.. autoattribute:: dyff.schema.platform.TagName

.. autopydantic_model:: dyff.schema.platform.TaskSchema
