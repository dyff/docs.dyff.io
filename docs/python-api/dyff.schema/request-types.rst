.. Cut the right ("Contents") sidebar at the "class name" level

:tocdepth: 3

.. py:module:: dyff.schema.requests

Request Types
=============

The Pydantic models in :py:mod:`dyff.schema.requests` define the data schema for requests sent to the Dyff system. You will typically encounter these types as arguments to Dyff API functions.

Resource Creation
-----------------

.. autopydantic_model:: dyff.schema.requests.AnalysisCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ConcernCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.DatasetCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.EvaluationCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.InferenceServiceCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.InferenceSessionCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.MethodCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ModelCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ModuleCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ReportCreateRequest
    :inherited-members: BaseModel, DyffBaseModel

Queries
-------

.. autopydantic_model:: dyff.schema.requests.DatasetQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.DocumentationQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.EvaluationQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.InferenceServiceQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.InferenceSessionQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.MeasurementQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.MethodQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ModelQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ModuleQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ReportQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.SafetyCaseQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.ScoreQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.UseCaseQueryRequest
    :inherited-members: BaseModel, DyffBaseModel

Metadata
--------

.. autopydantic_model:: dyff.schema.requests.DocumentationEditRequest
    :inherited-members: BaseModel, DyffBaseModel

.. autopydantic_model:: dyff.schema.requests.LabelUpdateRequest
    :inherited-members: BaseModel, DyffBaseModel
