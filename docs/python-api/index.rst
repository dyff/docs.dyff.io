Python SDK
==========

The Dyff Python SDK consists of several packages that can be used independently. The ``dyff`` meta-package installs compatible versions of all of the SDK packages.

.. toctree::
   :maxdepth: 1

   dyff
   dyff.audit/index
   dyff.client
   dyff.schema/index
