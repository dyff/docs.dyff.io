Grouping by UseCase and other concerns
======================================

The Dyff schema includes a special kind of "label" for resources called a :py:class:`~dyff.schema.platform.Concern` that is used to group resources according to well-known areas of concern, such as propensity to cause certain types of harms, or applicability to certain use-cases.

Currently, Dyff implements one kind of Concern called :py:class:`~dyff.schema.platform.UseCase`, but we intend to expand this taxonomy.

``UseCases``
------------

A :py:class:`~dyff.schema.platform.UseCase` is a kind of Concern that describes a concrete use-case (or application) of AI systems. The ``UseCase`` resource simply gives an ID to a description of a use case:

.. code-block:: python

    usecase = dyffapi.usecases.create(
        ConcernCreateRequest(
            title="Underwater basket-weaving",
            summary="Using AI to weave baskets while underwater",
        )
    )

.. note::

    Prefer to re-use existing use cases from the ``"public"`` account rather than creating your own. This makes querying by use case more effective for finding related resources.

Labeling with Concerns
----------------------

Use the ``.add_concern()`` and ``.remove_concern()`` API functions to label resources with appropriate concerns.

.. note::

    Currently, only :py:class:`~dyff.schema.platform.Method` resources can be labeled with concerns.

.. code-block:: python

    dyffapi.methods.add_concern("method-id", usecase)

Querying by Concern
-------------------

.. note::

    This example relies on internal details of the data representation that may change at any time.

    A future release will formalize the interface for querying by concern.

Internally, the ``.add_concern()`` function is adding a particular label to the resource. You can use this label to query resources by the concerns they are labeled with:

.. code-block:: python

    methods = dyffapi.methods.query(labels={"usecases.dyff.io/<usecase id>": "1"})

The label is formatted in this way so that a method can be labeled with multiple use-cases.
