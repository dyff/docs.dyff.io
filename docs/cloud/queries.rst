Organizing and querying resources
=================================

The Dyff API provides a query interface that lets you retrieve all resources satisfying given criteria.

Basic equality queries
----------------------

You can use the keyword arguments to the various ``.query()`` functions to retrieve all resources where the given fields are equal to the given values. For example, to get all :py:class:`Datasets <dyff.schema.platform.Dataset>` in the ``"public"`` account that are in ``"Ready"`` status, use the query:

.. code-block:: python

    dyffapi.datasets.query(account="public", status="Ready")

The query is a conjunction ("AND") over the specified equality constraints. Most top-level fields are queryable. Certain fields in nested objects are also exposed for queries, for convenience. For example, you can query :py:class:`Evaluations <dyff.schema.platform.Evaluation>` by the ID of the :py:class:`~dyff.schema.platform.InferenceService` they were run against:

.. code-block:: python

    dyffapi.evaluations.query(inferenceService="service-id")

This is acutally a query on the nested field ``Evaluation.inferenceSession.inferenceService.id``.

Disjunctive queries
-------------------

You can also query all entities where a field matches any of a set of values -- a disjunction ("OR") over values. Note that the overall query is still a conjunction-of-disjunctions.

To perform this kind of query, you must compose the query as a JSON object, stringify it, and pass it in the ``query=`` keyword argument:

.. code-block:: python

    import json

    evaluations = dyffapi.evaluations.query(
        query=json.dumps(
            {"account": ["account1", "account2"], "status": ["Completed", "Error"]}
        ),
        # You can still use other keyword arguments
        # The results must safisfy 'query' AND 'dataset'
        dataset="dataset-id",
    )

The queryable fields are the same as when using keyword arguments.

Order and limit
---------------

You can also sort the results, and limit the number of results returned:

.. code-block:: python

    newest5 = dyffapi.evaluations.query(orderBy="creationTime", order="descending", limit=5)

The order of operations is ``query -> sort -> limit``.

Queries and permissions
-----------------------

Queries will return all matching entities that your API token grants access to. If your token grants query permission for accounts that you share with others, the query may return more entities than you expected. It's a good idea to limit queries to specific accounts if you plan to process the results without inspecting them first.

Labels
------

You can label entities in Dyff with arbitrary key-value pairs (subject to format constraints). These follow the `Kubernetes label naming rules <https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set>`_. Keys that are prefixed with ``dyff.io/`` or ``subdomain.dyff.io/`` have special meaning for the Dyff platform. Keys without a prefix are for your personal use and have no effect on Dyff platform behavior. Labels that are added or consumed by third-party tools must use an appropriate key prefix.

For example, suppose you have a large family of datasets, and you want to organize them by color. You can label individual datasets with their color, and then later query datasets according to the label:

.. code-block:: python

    dyffapi.datasets.label("dataset1", {"color": "red", "size": "big"})
    dyffapi.datasets.label("dataset2", {"color": "red", "size": "small"})
    dyffapi.datasets.label("dataset3", {"color": "green", "size": "big"})
    dyffapi.datasets.query(account="my-account", labels={"color": "red"})
    # returns [dataset1, dataset2]

Keep in mind that other Dyff users might have used the same label keys to label their resources, and those resources will appear in the query result if you have permission to query them.
