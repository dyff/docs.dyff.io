from pathlib import Path

from dyff.audit.local import DyffLocalPlatform

account = "local"
root = Path("/home/me/dyff")

dyfflocal = DyffLocalPlatform(storage_root=root / ".dyff-local")

dataset = dyfflocal.datasets.create_arrow_dataset(
    str(root / "arrow_dataset"), account=account, name="test"
)
dyfflocal.datasets.upload_arrow_dataset(dataset, str(root / "arrow_dataset"))
print(dataset.json(indent=2))

evaluation_id = dyfflocal.evaluations.import_data(str(root / "outputs_dataset"))
print(f"Evaluation: {evaluation_id}")
