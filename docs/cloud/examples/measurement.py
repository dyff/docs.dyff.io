from __future__ import annotations

from pathlib import Path

import my_package

from dyff.audit.local import DyffLocalPlatform
from dyff.schema.dataset import arrow
from dyff.schema.platform import *
from dyff.schema.requests import *

ACCOUNT: str = ...
ROOT_DIR: Path = Path("/home/me/dyff")

# Develop using the local platform
dyffapi = DyffLocalPlatform(
    storage_root=ROOT_DIR / ".dyff-local",
)
# When you're ready, switch to the remote platform:
# dyffapi = Client(...)

module_root = str(ROOT_DIR / "my-module")
module = dyffapi.modules.create_package(
    module_root,
    account=ACCOUNT,
    name="my-module",
)
dyffapi.modules.upload_package(module, module_root)
print(module.json(indent=2))

method_request = MethodCreateRequest(
    name="mean-word-length",
    # The analysis results describe one Evaluation
    scope=MethodScope.Evaluation,
    description="Computes the mean length of words in the input and output datasets.",
    # The method is implemented as the python function 'my_package.word_count()'
    implementation=MethodImplementation(
        kind=MethodImplementationKind.PythonFunction,
        pythonFunction=MethodImplementationPythonFunction(
            fullyQualifiedName="my_package.word_count",
        ),
    ),
    # The method accepts one argument called 'wordOfTheDay'
    parameters=[
        MethodParameter(keyword="wordOfTheDay", description="A cromulent word"),
    ],
    # The method accepts two PyArrow datasets as inputs:
    # - The one called 'prompts' is from a Dataset resource (i.e., system inputs)
    # - The one called 'completions' is from an Evaluation resource (system outputs)
    inputs=[
        MethodInput(kind=MethodInputKind.Dataset, keyword="prompts"),
        MethodInput(kind=MethodInputKind.Evaluation, keyword="completions"),
    ],
    # The method produces a Measurement
    output=MethodOutput(
        kind=MethodOutputKind.Measurement,
        measurement=MeasurementSpec(
            name="mean-word-length",
            description="The mean length of words in the input and output datasets.",
            # There is (at least) one row per input (_index_ x _replication_)
            level=MeasurementLevel.Instance,
            # This is the schema of the output
            schema=DataSchema(
                arrowSchema=arrow.encode_schema(
                    arrow.arrow_schema(my_package.WordLengthScoredItem)
                ),
            ),
        ),
    ),
    # The Module containing 'my_package'
    modules=[module.id],
    account=ACCOUNT,
)
method = dyffapi.methods.create(method_request)
print(method.json(indent=2))

dataset_id: str = ...
evaluation_id: str = ...
analysis_request = AnalysisCreateRequest(
    account=ACCOUNT,
    method=method.id,
    arguments=[
        AnalysisArgument(keyword="wordOfTheDay", value="embiggen"),
    ],
    inputs=[
        AnalysisInput(keyword="prompts", entity=dataset_id),
        AnalysisInput(keyword="completions", entity=evaluation_id),
    ],
)
measurement = dyffapi.measurements.create(analysis_request)
print(measurement.json(indent=2))
