from pathlib import Path

from dyff.audit.local import DyffLocalPlatform
from dyff.client import Client

account = "local"
root = Path("/home/me/dyff/my-analysis")

dyffremote = Client(...)
dyfflocal = DyffLocalPlatform(
    storage_root=root / ".dyff-local", remote_client=dyffremote
)

dataset = dyfflocal.datasets.create_arrow_dataset(
    str(root / "arrow_dataset"), account=account, name="test"
)
dyfflocal.datasets.upload_arrow_dataset(dataset, str(root / "arrow_dataset"))

inferencesession_id = ...
evaluation_id = dyfflocal.evaluations.local_evaluation(
    dataset=dataset.id, inferencesession=inferencesession_id
)
