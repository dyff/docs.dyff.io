from dyff.schema.platform import (
    Accelerator,
    AcceleratorGPU,
    ContainerImageSource,
    DataSchema,
    DyffDataSchema,
    InferenceInterface,
    InferenceServiceRunner,
    InferenceServiceRunnerKind,
    ModelResources,
    SchemaAdapter,
)
from dyff.schema.requests import InferenceServiceCreateRequest

model_name = "facebook/opt-125m"
model_id = "9e96671f943c42bfa7f495ea5eb063f8"
service_name = f"{model_name}/openai"

runner = InferenceServiceRunner(
    kind=InferenceServiceRunnerKind.VLLM,
    image=ContainerImageSource(
        host="registry.gitlab.com",
        name="dyff/workflows/vllm-runner",
        digest="sha256:6a5107db7bb5dac4d231f5c0a97f34d95c17bda40ff6428b00e1c621352eb125",
        tag="0.6.3",
    ),
    # Command line args; format is specific to the runner
    args=[
        # T4 GPUs don't support the 'bfloat16' format this model defaults to
        "--dtype",
        "float16",
        # The OpenAI interface requires us to specify name(s) for the served model
        "--served-model-name",
        model_id,
        model_name,
    ],
    accelerator=Accelerator(
        kind="GPU",
        gpu=AcceleratorGPU(
            hardwareTypes=["nvidia.com/gpu-t4"],
            count=1,
        ),
    ),
    resources=ModelResources(
        storage="300Mi",
        memory="8Gi",
    ),
)

interface = InferenceInterface(
    # The inference endpoint served by the runner container
    endpoint="openai/v1/completions",
    # The output records should look like: {"text": "To be, or not to be"}
    outputSchema=DataSchema.make_output_schema(
        DyffDataSchema(
            components=["text.Text"],
        ),
    ),
    # How to convert inputs to the service to the format the runner expects
    # Input: {"text": "The question"}
    # Adapted: {"prompt": "The question", "model": <model_id>}
    inputPipeline=[
        SchemaAdapter(
            kind="TransformJSON",
            configuration={"prompt": "$.text", "model": model_id},
        ),
    ],
    # How to convert the runner output to match outputSchema
    # Output: {"choices": ["that is the question", "that is a tautology"]}
    # Adapted: [{"text": "that is the question"}, {"text": "that is a tautology"}]
    outputPipeline=[
        SchemaAdapter(
            kind="ExplodeCollections",
            configuration={"collections": ["choices"]},
        ),
        SchemaAdapter(
            kind="TransformJSON",
            configuration={"text": "$.choices.text"},
        ),
    ],
)

service_request = InferenceServiceCreateRequest(
    account="example",
    name=service_name,
    model=model_id,
    runner=runner,
    interface=interface,
)
