Dyff Cloud Tutorial
===================

.. toctree::
    :hidden:
    :caption: Dyff Cloud Tutorial

    queries
    groups
    bulk-requests
    integration
    advanced-services

This tutorial covers additional features that require interacting with a full-featured Dyff platform instance.

You must use the Dyff API :py:class:`~dyff.client.Client` to complete this tutorial. You will need an API token that grants the necessary permissions.

If you are a UL DSRI research collaborator, you can request a user account on the Dyff instance operated by DSRI.

Alternatively, if you are running Ubuntu Linux or something similar, you can deploy a :doc:`local cluster <../deploy/local-cluster/index>`.
