Connect
=======

Connect to Dyff
---------------

.. tab:: Dyff Cloud

    .. card::

        Use :py:class:`~dyff.client.Client` to connect to Dyff Cloud at `api.dyff.io/v0
        <https://api.dyff.io/v0>`_:


        .. warning::

            Use of Dyff Cloud is currently restricted to research collaborators.
            Email `contact@dsri.org <mailto:contact@dsri.org>`_ to obtain an API
            key.

        Provide an API token:

        .. code:: bash

            $ export DYFF_API_TOKEN="<your API token>"

        Use the client:

        >>> from dyff.client import Client
        >>> dyffapi = Client()



.. tab:: Self-hosted cluster

    .. card::

        Use :py:class:`~dyff.client.Client` with the ``endpoint`` parameter to
        connect to another Dyff instance:

        >>> from dyff.client import Client
        >>> dyffapi = Client(
        ...     endpoint="https://dyff.example.com/v0",
        ... )

        You can set the ``DYFF_API_ENDPOINT`` environment variable instead.

.. tab:: Local cluster

    .. card::

        When using a :doc:`local cluster <deploy/local-cluster/index>`, use
        :py:class:`~dyff.client.Client` with a custom endpoint and add
        ``insecure=True`` to allow self-signed certificates:

        .. warning::

            The ``insecure`` flag should **not** be used in production.

        >>> from dyff.client import Client
        >>> dyffapi = Client(
        ...     endpoint="https://api.dyff.local/v0",
        ...     insecure=True,
        ... )

.. tab:: Without cluster

    .. card::

        A subset of Dyff functionality is available without a Dyff cluster using
        :py:class:`~dyff.audit.local.DyffLocalPlatform`.

        .. doctest::
            :skipif: True

            >>> from dyff.audit.local import DyffLocalPlatform
            >>> dyffapi = DyffLocalPlatform()

        Artifacts save to ``./dyff-outputs`` by default. Pass ``storage_root``
        to use another directory:

        .. doctest::
            :skipif: True

            >>> dyffapi = DyffLocalPlatform(storage_root="/path/to/outputs")

        The local platform is useful for developing new tests.

Test the client
---------------

You should now be able to execute API operations. For example, you can list all
of the ``Models`` in the system that your API token grants access to:

.. doctest::
    :skipif: True

    >>> dyffapi.models.query()
    [<list of dyff.schema.platform.Model objects>]

Troubleshooting
---------------

    401 Unauthorized error
        You have not provided a valid API token. If you're providing credentials
        via environment variables (as we recommend), check that the variable
        ``DYFF_API_TOKEN`` is set correctly. A Dyff API token looks like a long string of base64 characters.

    403 Forbidden error
        You have provided a valid API token, but it does not grant you
        permission to take the action you've taken. By default, many operations
        -- like ``.create()`` and ``.data()`` -- are permitted only for
        resources in your account.

    404 Not found
        A resource ID was not found in the system. For ``.create()`` operations, "not found" means that a referenced resource that is consumed by the created operation was not found. If you provided a valid ID, this might mean that the referenced resource has been permanently deleted. Newly-created resources may appear as "not found" for a short time after being created (the Dyff datastore is not "read-after-write" consistent).
