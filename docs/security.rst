Security
========

.. toctree::
    :hidden:

    rbac

.. tip::

    This page describes Dyff Cloud but can also be achieved with a self-hosted
    Dyff deployment.

TLS authentication
------------------

The external API is HTTPS-only. We use `cert-manager
<https://cert-manager.io/>`_ and `Let's Encrypt <https://letsencrypt.org/>`_ to
issue SSL certificates.

Least-privilege service accounts
--------------------------------

All services run with fine-grained service accounts that grant them the minimal
permissions they need to accomplish their tasks.

Token-based authentication
--------------------------

API clients authenticate with bearer tokens. These are JWT tokens that contain a
cryptographically-signed list of access grants. Tokens for user accounts contain
a secret that can be compared to the authorization database. This allows for
revoking specific user tokens. Other services use ephemeral tokens, which do not
contain a secret and cannot be revoked, but which generally have a short valid
lifetime.

Role-based access control (RBAC)
--------------------------------

Tokens grant permissions using an RBAC system. Access to API endpoints is
granted by resource, by resource owner, by resource type, and by function. For
example, a token might grant permission to:

    1. ``create`` (function) an ``Evaluation`` (type) in account ``"myaccount"``
       (owner);

    2. ``consume`` (function) any ``Dataset`` (type) owned by account
       ``"public"`` (owner); and

    3. ``consume`` (function) the ``InferenceService`` (type) with ID
       ``"abc123"`` (resource).

Together, these grants are sufficient to evaluate ``"abc123"`` on any
``"public"`` dataset, using computational resources allocated to
``"myaccount"``.
