# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

PYTHON_VERSION = 3.9
VENV ?= venv
UV ?= uv

UV_PYTHON = $(PYTHON_VERSION)
VIRTUAL_ENV = $(VENV)
PYTHON ?= $(VENV)/bin/python3
SPHINX_BUILD = $(VENV)/bin/sphinx-build
SPHINX_AUTOBUILD = $(VENV)/bin/sphinx-autobuild

export UV_PYTHON VIRTUAL_ENV

BASE_DIR = $(shell pwd)
SPHINXOPTS ?= # -W

APT_INSTALL = apt-get install -y --no-install-recommends
APT_UPDATE = apt-get update -y

APT_PACKAGES = graphviz pandoc

APT_TARGETS = $(patsubst %,$(VENV)/pkg/%,$(APT_PACKAGES))

SPHINX_BUILDERS = dirhtml linkcheck doctest latexpdf

.PHONY: all
all: setup

.PHONY: setup
setup: $(VENV)/requirements.txt;

.PHONY: setup-apt
setup-apt: $(APT_TARGETS)

$(VENV)/pkg/%: | $(VENV)/pkg/.update
	$(APT_INSTALL) "$(notdir $@)"
	touch "$(VENV)/pkg/$(notdir $@)"

$(VENV)/pkg/.update: | $(VENV)/pkg
	$(APT_UPDATE)
	touch $(VENV)/pkg/.update

requirements.txt: requirements.in | $(VENV)
	rm -f requirements.txt
	uv pip compile --python-version $(PYTHON_VERSION) --upgrade -o "$@" "$<"

$(VENV)/requirements.txt: requirements.txt | $(VENV)
	$(UV) pip install -r requirements.txt
	cp -f requirements.txt $(VENV)/requirements.txt

$(VENV)/pkg: | $(VENV)
	mkdir $(VENV)/pkg

$(VENV):
	uv venv $(VENV)

.PHONY: compile
compile:
	uv pip compile --python-version $(PYTHON_VERSION) --upgrade -o requirements.txt requirements.in

.PHONY: distclean
distclean:
	rm -rf node_modules/ $(VENV)

.PHONY: build
build:
	$(MAKE) -C docs dirhtml -e SPHINXOPTS="$(SPHINXOPTS)"

.PHONY: clean
clean:
	rm -rf docs/_build

.PHONY: clean-dirhtml
clean-dirhtml:
	rm -rf docs/_build/dirhtml

.PHONY: serve
serve:
	$(PYTHON) -m http.server --directory docs/_build/dirhtml

.PHONY: autobuild
autobuild:
	$(SPHINX_AUTOBUILD) -j auto -b dirhtml docs docs/_build/dirhtml

.PHONY: $(SPHINX_BUILDERS)
$(SPHINX_BUILDERS):
	$(MAKE) -C docs $@ -e SPHINXOPTS="$(SPHINXOPTS)"

DRAWIO_FILES = $(shell find -name \*.drawio)
DRAWIO_SVG_FILES = $(patsubst %.drawio,%.svg,$(DRAWIO_FILES))

.PHONY: drawio
drawio: $(DRAWIO_SVG_FILES)

%.svg: %.drawio
	drawio -x -f svg "$<" -o "$@"

.PHONY: clean-drawio
clean-drawio:
	rm -f $(DRAWIO_SVG_FILES)
