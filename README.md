# docs.dyff.io website

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/dyff/docs.dyff.io?branch=main)](https://gitlab.com/dyff/docs.dyff.io/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![formatter: docformatter](https://img.shields.io/badge/formatter-docformatter-fedcba.svg)](https://github.com/PyCQA/docformatter)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

Source code for the documentation hosted at [docs.dyff.io](https://docs.dyff.io/).

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Setup

[uv](https://github.com/astral-sh/uv) is required by the makefile.

```bash
python3 -m pip install uv
```

Setup a virtualenv at `venv` and install the Python dependencies:

```bash
make setup
```

## Usage

Build the docs for release:

```bash
make build
```

A web server can be started:

```bash
make serve
```

The docs can then be viewed locally at <http://localhost:8000>.

A live-reloading version of the docs is also available:

```bash
make autobuild
```

Remove build artifacts:

```bash
make clean
```

Fully remove the virtualenv:

```bash
make distclean
```

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
